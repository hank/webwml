# translation of others.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007.
# Mirosław Gabruś <mirekgab@wp.pl>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: others.pl\n"
"PO-Revision-Date: 2014-08-01 09:22+0200\n"
"Last-Translator: Mirosław Gabruś <mirekgab@wp.pl>\n"
"Language-Team: polski <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Kącik nowych członków"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Krok 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Krok 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Krok 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Krok 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Krok 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Krok 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Krok 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Lista spraw"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Więcej informacji (tylko w języku francuskim) dostępnych jest pod adresem <a "
"href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/index.fr."
"html</a>."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Więcej informacji"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Więcej informacji (tylko w języku hiszpańskim) dostępnych jest pod adresem "
"<a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</a>."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefon"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adres"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produkty"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Koszulki"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "czapeczki"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "naklejki"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "kubki"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "inne części ubioru"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "koszulki polo"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbee"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "podkładki pod mysz"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "naszywki"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "kosze do koszykówki"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "kolczyki"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "torby"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "parasole"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "poszewki na poduszki"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "breloki"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Szwajcarski scyzoryk"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "Pamięci USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Z&nbsp;napisem&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Bez&nbsp;napisu&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (miniprzycisk)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "jak wyżej"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Jak długo używasz Debiana?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Czy jesteś Deweloperem Debiana"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "W jaką część Debiana jesteś zaangażowana(y)?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Jaką pracą dla Debiana jesteś zainteresowana(y)?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Czy masz jakieś rady dla kobiet zainteresowanych większym zaangażowaniem się "
"w projekty Debiana?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Czy jesteś zaangażowana w innych technologicznych grupach dla kobiet? W "
"jakich?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Trochę więcej informacji o Tobie ..."

#~ msgid "Where:"
#~ msgstr "Gdzie:"

#~ msgid "Specifications:"
#~ msgstr "Specyfikacje:"

#~ msgid "Architecture:"
#~ msgstr "Architektura:"

#~ msgid "Who:"
#~ msgstr "Kto:"

#~ msgid "Wanted:"
#~ msgstr "Pożądane:"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Version"
#~ msgstr "Wersja"

#~ msgid "Status"
#~ msgstr "Stan"

#~ msgid "Package"
#~ msgstr "Pakiet"

#~ msgid "ALL"
#~ msgstr "Wszystkie"

#~ msgid "Unknown"
#~ msgstr "Nieznany"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "ZŁY?"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD"
#~ msgstr "ZŁY"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Old banner ads"
#~ msgstr "Stare banery"

#~ msgid "Download"
#~ msgstr "Pobierz"

#~ msgid "Unavailable"
#~ msgstr "Niedostępne"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Nieznane"

#~ msgid "No images"
#~ msgstr "Brak obrazów"

#~ msgid "No kernel"
#~ msgstr "Brak jądra"

#~ msgid "Not yet"
#~ msgstr "Jeszcze nie gotowe"

#~ msgid "Building"
#~ msgstr "Buduje się"

#~ msgid "Booting"
#~ msgstr "Uruchamia się"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (uszkodzone)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "Działające"
