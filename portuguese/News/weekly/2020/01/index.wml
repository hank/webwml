#use wml::debian::projectnews::header PUBDATE="2020-09-09" SUMMARY="Boas-vindas ao DPN, canais de comunicação Debian, novo DPL, notícias internas e externas, DebConf20 e eventos, relatórios e chamadas para ajuda"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="de00676864ba33b45c4e42789e4ea68c0bacd001"

# Status: [Frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="primeiro" />

<toc-add-entry name="newdpn">Boas-vindas às Notícias do Projeto Debian!</toc-add-entry>

<p>Esperamos que você aproveite esta <b>edição especial histórica</b> das 
Notícias do Projeto Debian que repetem, introduzem e tentam atualizar a maioria
das notícias para o ano de 2020 até o momento.</p>

<p>
Para esta edição especial das notícias, omitimos algumas seções regulares
em um esforço para trazer nossa cobertura mais atualizada. Caso deseje encontrar
informações mais antigas ou ausentes, por favor, reveja nosso 
<a href="https://bits.debian.org">blog Bits do Debian</a> ou o
<a href="https://micronews.debian.org">serviço Micronotícias do Debian (Micronews)</a>
onde todas as notícias recentes e atuais já foram compartilhadas ao longo deste
ano.
</p>

<toc-add-entry name="official">Canais de comunicação oficial do Debian.</toc-add-entry>

<p>
De tempos em tempos recebemos perguntas sobre eventos atuais, as regras sobre 
quem pode ter páginas web no domínio Debian, o andamento do desenvolvimento 
dentro da comunidade, ou dúvidas sobre os canais oficiais de comunicação com e
do Debian.
</p>

<p>
A seção <a href="https://www.debian.org/News/">Notícias</a> do site web
do Debian traz informações e anúncios formais, muitos dos quais são 
espelhados/compartilhados nas listas de discussão debian-news e debian-announce.
</p>

<p>
O <a href="https://bits.debian.org/">blog Bits do Debian</a> traz notícias de
diversas listas de discussão do Debian juntamente com informações e anúncios 
semiformais em um ciclo de atualizações muito mais rápido.
</p>

<p>
O <a href="https://micronews.debian.org/">serviço de Micronotícias (Micronews)</a>
traz notícias e cobre artigos curtos, de impacto imediato, ou notícias de
última hora e de eventos atuais. O serviço de Micronotícias também alimenta
várias mídias sociais, tais como twitter, identi.ca e framapiaf.org.
</p>

<p>
Naturalmente não podemos esquecer o meio pelo qual você está lendo: as 
<a href="https://www.debian.org/News/weekly/">Notícias do Projeto Debian</a>
é o boletim informativo do projeto, que é publicado de forma um pouco aleatória 
(ajuda necessária - ver abaixo).
</p>

<p>
Por favor observe que todos os canais de comunicação listados estão disponíveis 
através do protocolo <a href="https://en.wikipedia.org/wiki/HTTPS"><b>https://</b></a> 
que você verá em uso por todos nossos sites web e canais oficiais de comunicação.
</p>

<p>
Caso você precise entrar em contato conosco para perguntas sobre um anúncio ou 
item de notícia, por favor, não hesite em nos contatar via e-mail em
<a href="mailto:press@debian.org">press@debian.org</a>. Pedimos a você que
por favor note que as equipes de imprensa e publicidade que <i>provêm estes
serviços apenas de informação</i> não são capazes de fornecer suporte direto
ao(à) usuário(a); tais consultas devem ser encaminhadas para as 
<a href="https://lists.debian.org/debian-user/">listas de discussão debian-user</a>
ou para a <a href="https://wiki.debian.org/Teams">equipe Debian</a> apropriada.
</p>

<toc-add-entry name="helpspread">A equipe de publicidade chama por
voluntários(as) e ajuda!</toc-add-entry>

<p>A equipe de publicidade está pedindo a ajuda de nossos(as) leitores(as), 
desenvolvedores(as) e partes interessadas para contribuir para o esforço de
notícias do Debian. Imploramos que você envie itens que possam ser de interesse
para nossa comunidade e também solicitamos sua ajuda com traduções das notícias
em (seu!) outro idioma junto com o segundo ou terceiro par de olhos
necessários para ajudar na produção de nosso trabalho antes publicação. Se você
puder compartilhar uma pequena parte de seu tempo para ajudar nossa equipe que
esforça-se para manter todos(as) nós informados(as), precisamos de você. Por
favor, entre em contato conosco através do IRC em
<a href="irc://irc.debian.org/debian-publicity">&#35;debian-publicity</a> no
<a href="https://oftc.net/">OFTC.net</a>, na
<a href="mailto:debian-publicity@lists.debian.org">lista de discussão pública</a>
ou por e-mail em <a href="mailto:press@debian.org">press@debian.org</a> para
questões sensíveis ou privadas.</p>

<toc-add-entry name="longlivetheking">Elegemos um novo DPL.</toc-add-entry>

<p>Jonathan Carter (highvoltage), o recém-eleito DPL, compartilha suas ideias,
agradecimentos e esboços para o futuro do Debian em sua primeira postagem
oficial no
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html">blog</a>
como DPL. Novos posts do blog DPL também são <a href="https://bits.debian.org/dpl/">postados</a>
no blog oficial do Debian.
</p>

<toc-display/>

<toc-add-entry name="security">Avisos de segurança</toc-add-entry>

<p>A equipe de segurança do Debian divulga alertas atuais diariamente em
(<a href="$(HOME)/security/2020/">alertas de segurança 2020</a>). Por favor, 
leia-os com atenção e assine a <a href="https://lists.debian.org/debian-security-announce/">lista de discussão de segurança</a>.</p>

<p>O site web do Debian agora também <a href="https://www.debian.org/lts/security/">arquiva</a>
avisos de segurança lançados pela equipe de suporte de longo prazo 
e publicados na <a href="https://lists.debian.org/debian-lts-announce/">lista de discussão debian-lts-announce</a>.
</p>

<toc-add-entry name="internal">Notícias internas</toc-add-entry>

<p><b>Notícias para o Debian 10 <q>buster</q>: 10.5 lançado</b></p>

<p>Em julho de 2019, demos boas-vindas ao
<a href="https://www.debian.org/News/2019/20190706">lançamento do Debian 10
(codename <q>buster</q>)</a>. Desde então, o projeto Debian anunciou a
<a href="https://www.debian.org/News/2019/20190907">primeira</a>,
<a href="https://www.debian.org/News/2019/20191116">segunda</a>,
<a href="https://www.debian.org/News/2020/20200208">terceira</a>,
<a href="https://www.debian.org/News/2020/20200509">quarta</a>
e <a href="https://www.debian.org/News/2020/20200801">quinta</a>
versões pontuais da versão estável (stable).
O Debian 10.5 foi publicado em 1 de agosto 2020.</p>

<p><b>Atualizado Debian 9: 9.13 lançado</b></p>

<p>O Projeto Debian anunciou a
<a href="https://www.debian.org/News/2019/2019090702">décima</a>,
<a href="https://www.debian.org/News/2019/20190908">décima primeira</a>,
<a href="https://www.debian.org/News/2020/2020020802">decima segunda</a>,
e <a href="https://www.debian.org/News/2020/20200718">decima terceira</a>
versões pontuais (e final) de sua versão antiga (oldstable)
Debian 9 (codename <q>stretch</q>).
O Debian 9.13 foi publicado em 18 de julho 2020.</p>

<p>O suporte de segurança para a versão <q>antiga (oldstable)</q> Debian 9 foi
encerrado em 6 de julho de 2020. Entretanto, a <q>stretch</q> se beneficua do
suporte de longo prazo (LTS)
<a href="https://wiki.debian.org/LTS/Stretch">até o fim de junho de 2022</a>.
O LTS é limitado as arquiteturas i386, amd64, armel, armhf e arm64. 
</p>

<p><b>Debian 8 LTS chegou ao fim da vida</b></p>

<p>Em 30 de junho de 2020, o suporte LTS para o Debian 8 <q>Jessie</q> chegou ao 
fim da vida 5 anos após seu lançamento em 26 de abril de 2015.</p>

<p><b>Novidades sobre o Debian 11 <q>bullseye</q></b></p>

<p>Paul Gevers da equipe de lançamento 
<a href="https://lists.debian.org/debian-devel-announce/2020/03/msg00002.html">compartilhou</a>
a política atualizada para nosso próximo lançamento, as datas provisórias de 
congelamento, juntamente com outras atualizações e mudanças. Para informações 
adicionais sobre como essas mudanças e políticas serão, por favor, 
consulte os <a
href="https://lists.debian.org/debian-devel-announce/2019/07/msg00002.html">Bits da equipe de lançamento</a>.</p>

<p>Aurelien Jarno propôs 
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html"> a remoção da arquitetura mips</a>
para o próximo lançamento, porque o esforço para portar tem se tornado cada vez
mais difícil. Esta remoção não afeta <q>stretch</q> ou <q>buster</q>.
</p>

<p>Em razão da remoção do Python 2 da versão <q>bullseye</q>, a equipe 
debian-python fornece notícias sobre o <a
href="https://lists.debian.org/debian-devel-announce/2019/11/msg00000.html">progresso e próximos passos</a>
do processo. Mais detalhes estão disponíveis na <a
href="https://wiki.debian.org/Python/2Removal">página wiki dedicada</a>.</p>

<p>O Instalador Debian Bullseye versão Alpha 1 foi 
<a href="https://www.debian.org/devel/debian-installer/News/2019/20191205.html">publicado</a>
em 5 de dezembro de 2019.</p>

<p>Cyril Brulebois compartilhou novidades sobre o lançamento do <a
href="https://lists.debian.org/debian-devel-announce/2020/03/msg00005.html">Instalador Debian Bullseye versão Alpha 2</a>
publicado em 16 de março de 2020, apresentando muitas melhorias e mudanças para
a configuração de relógio, grub-installer, preseed e systemd, entre outras 
melhorias. Testes e relatórios para encontrar bugs e melhorar ainda mais o 
instalador são bem-vindos. Imagens do instalador e tudo mais estão
<a href="https://www.debian.org/devel/debian-installer">disponíveis para download</a>.</p>

<p><b>Um novo pacote de localização para as páginas de manual (manpages)</b></p>

<p>Historicamente, as páginas de manual para um determinado pacote têm 
recebido frequentemente traduções fornecidas em vários outros pacotes, 
utilizando diferentes métodos, equipes e formatos. Atualmente, uma grande parte 
delas não é mais mantida. A maioria das páginas de manual críticas foram 
previamente traduzidas para manter as páginas de manual atualizadas.</p>

<p>O Debian vai lançar um novo pacote
<a href="https://lists.debian.org/debian-l10n-french/2020/02/msg00105.html">manpages-l10n</a>
que irá substituir Perkamon, manpages-de, manpages-fr, manpages-fr-extra, 
manpages-pl, manpages-pl-dev, manpages-pt e manpages-pt-dev. Além disso, este 
novo pacote traz traduções manpage para os idiomas holandês e romeno.  

O resultado será que o francês, alemão, holandês, polonês, romeno e os falantes
de português terão agora possivelmente traduzido as páginas de manual contidas 
em um único pacote. A partir dessas traduções, os falantes destes idiomas 
poderão ajudar ou relatar bugs sobre as páginas. Damos as boas-vindas a
outras pessoas para se juntarem a este esforço, mas por favor tenham em mente as
áreas onde os pacotes não são mais mantidos, esses pacotes têm uma prioridade
maior para converter em arquivos PO e atualizar.</p>

<p><b>Chaves DKIM </b></p>

<p>Adam D. Barratt compartilhou informações em um recente aprimoramento na 
infraestrutura Debian com a melhoria para os(as) DDs utilizarem 
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00004.html">chaves DKIM 
para a autenticação de e-mail</a>.</p>

<p><b>Mudanças na autenticação do Salsa</b></p>

<p>Enrico Zini anunciou que os <a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00007.html">logins no Salsa estão habilitados no nm.debian.org</a>.
Esta mudança melhora a facilidade de uso para os candidatos(as),
mantenedores(as) e novos(as) Desenvolvedores(as).
Após esta mudança, Bastian Blank, da equipe de administradores(as) do Salsa,
anunciou uma
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00010.html">atualização do Salsa: remoção de contas -guest</a>
para utilizar Salsa como um provedor de autenticação.</p>

<p>Sean Whitton divulgou a <a href="https://www.debian.org/doc/debian-policy/">política Debian 4.5.0.2</a>
com atualizações para nomes de usuários(as) gerados, uso de scripts para unidades de 
serviço e atualizações para update-rc.d.</p>


<p><b>Bits do Comitê Técnico</b></p>

<p>O comitê técnico, enquanto estava preparando sua palestra para a DebConf20
intitulada
<a href="https://debconf20.debconf.org/talks/16-meet-the-technical-committee/">conheça o comitê técnico</a>,
divulgou o seu 
<a href="https://lists.debian.org/debian-devel-announce/2020/08/msg00005.html">relatório anual</a>
e um <a href="https://salsa.debian.org/debian/tech-ctte/-/blob/master/talks/rethinking-the-tc.md">documento-proposta</a>
para a melhoria dos processos e trabalho do comitê.
</p>

<p><b>Nova equipe acadêmica do Debian</b></p>

<p>
Uma nova iniciativa para definir e rodar uma plataforma oficial de ensino 
digital do Debian com cursos específicos da distribuição. Quer ajudar a fazer 
isto acontecer, visite a  
<a href="https://wiki.debian.org/DebianAcademy">página wiki</a>
e junte-se à equipe.
</p>

<toc-add-entry name="external">Notícias externas</toc-add-entry>

<p><b>Debian France renovou sua diretoria</b></p>

<p>A Debian France é uma organização sem fins lucrativos que faz parte das 
organizações confiáveis. Ela é a principal organização confiável na Europa. 
Ela recentemente renovou sua diretoria. Foram poucas mudanças, exceto o 
presidente, que agora é Jean-Philippe MENGUAL.</p>

<p>Tenha em mente que esta organização sem fins lucrativos tem em sua diretoria 
apenas desenvolvedores(as) Debian, como Presidente(a), Secretário(a) e
Tesoureiro(a). Os(as) outros(as) administradores(as) não são
desenvolvedores(as) Debian, mas contribuem para o Debian em estandes ou outros
eventos.
<a href="https://france.debian.net/posts/2020/renouvellement_instances/">Veja as notícias</a>.
</p>


<toc-add-entry name="MiniDebs">MiniDebConfs, MiniDebCamps e DebConf20</toc-add-entry>

<p><b>MiniDebConfs e MiniDebCamps</b></p>
<p>
No início de 2020, houve sete <a href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a>
programadas em todo o mundo, refletindo a vitalidade de nossa comunidade.
Organizada por participantes do projeto Debian, as MiniDebConfs estão abertas a
todos(as) e proporcionam uma oportunidade para desenvolvedores(as),
colaboradores(as) e outras pessoas interessadas se encontrarem pessoalmente. Mas
devido às restrições com a pandemia de coronavírus (COVID-19), a maioria, senão
todas as conferências foram canceladas ou adiadas para 2021: 

<a href="https://maceio2020.debian.net">Maceió (Brasil)</a>,
<a href="https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen">Aberdeen (Escócia)</a>,
<a href="https://wiki.debian.org/DebianEvents/fr/2020/Bordeaux">Bordeaux (França)</a>,
<a href="https://wiki.debian.org/DebianLatinoamerica/2020/MiniDebConfLatAm">El Salvador</a> e
<a href="https://lists.debian.org/debian-devel-announce/2020/02/msg00006.html">Regensburg (Alemanha)</a>.
Algumas foram remarcadas online: a primeira,
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline">MiniDeconfOnline #1</a>,
precedida por um DebCamp, foi realizada de 28 a 31 de maio de 2020. A 
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">MiniDebConf Online #2 "Gaming Edition"</a>,
dedicada aos jogos no Debian (e no Linux em geral) acontecerá de 19
a 22 de novembro de 2020 - sendo os dois primeiros dias uma Debcamp. Visite a
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">página wiki do evento</a>
para detalhes.</p>


<p><b>DebConf20 foi realizada online, DebConf21 será realizada em Haifa, Israel</b></p>

<p>
A <a href="https://www.debian.org/News/2020/20200830">DebConf20</a> foi
realizada online pela primeira vez, devido a pandemia do coronavírus(COVID-19).
</p>

<p>A comunidade Debian adaptou-se a esta mudança com o contínuo compartilhamento
de ideias, sessões de "desconferências" (Birds of a Feather - BoF), discussões e
todas as coisas que desenvolvemos como tradições ao longo dos anos. Todas as
sessões foram transmitidas, com uma variedade de formas de participação: através
de mensagens no IRC, documentos de texto colaborativos online e salas para
reuniões por videoconferência.</p> 

<p>Com mais de 850 participantes de 80 países diferentes e um total de mais de 
100 palestras, sessões de discussão, "desconferências" Birds of a Feather (BoF) e 
outras atividades, a <a href="https://debconf20.debconf.org">DebConf20</a> foi um
grande sucesso. A agenda da DebConf20 incluiu duas trilhas em idiomas diferentes 
do inglês. A MiniConf em Espanhol e a Miniconf em Malaio.</p>

<p>A maioria das palestras e sessões estão disponíveis no <a
href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">site web com arquivos dos encontros Debian</a>,
e o <a href="https://debconf20.debconf.org/">site web DebConf20</a> permanecerá 
ativo para fins de histórico e continuará a oferecer links para 
apresentações e vídeos das palestras e eventos.
</p>

<p>
No próximo ano, a <a href="https://wiki.debian.org/DebConf/21">DebConf21</a>
está planejada para ser realizada em Haifa, Israel, em agosto ou setembro.
</p>

<toc-add-entry name="debday">Dia do Debian</toc-add-entry>

<p>27 anos forte! Feliz aniversário! Feliz <a href="https://wiki.debian.org/DebianDay/2020">#DebianDay!</a>
Devido à pandemia mundial de Covid-19, poucos eventos locais foram organizados 
para o DebianDay e alguns serão realizados online no <a href="https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2020">Brasil</a>
(15 e 16 de agosto). Ainda assim, desenvolvedores(as) e colaboradores(as)
tiveram a oportunidade de se encontrarem em La Paz (Bolívia), České Budějovice
(República Checa) e Haifa (Israel) durante a DebConf20.
</p>

<toc-add-entry name="otherevents">Outros eventos</toc-add-entry>

<p>
A comunidade Debian Brasil organizou de 3 de maio a 6 de junho um 
evento chamado
<a href="https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/">#FiqueEmCasaUseDebian</a>.
Durante 27 noites, os DDs Daniel Lenharo (lenharo) e Paulo Santana (phls)
receberam convidados(as) que compartilharam seus conhecimentos sobre o Debian.
Você pode ler um
<a href="http://softwarelivre.org/debianbrasil/blog/fiqueemcasausedebian-it-was-35-days-with-talks-translations-and-packaging">relato</a>.
</p>

<toc-add-entry name="reports">Informes</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>Informe mensal LTS Freexian</b></p>

<p>A Freexian emite <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">relatórios mensais</a>
sobre o trabalho de colaboradores(as) pagos(as) para a equipe de suporte a longo
prazo do Debian.
</p>

<p><b>Atualização de status das construções reprodutíveis</b></p>

<p>Siga o <a
href="https://reproducible-builds.org/blog/">blog Reproducible
Builds</a> para informações semanais deste trabalho no ciclo <q>buster</q>.
</p>


<toc-add-entry name="help">Precisa-se de ajuda</toc-add-entry>
#Make bold -fix
<p><b>Equipes precisando de ajuda</b></p>
## Teams needing help
## $Link to the email from $Team requesting help

<p>A referência para desenvolvedores(as) Debian é agora mantida como
ReStructuredText. Traduções e atualizações de traduções são bem-vindas - por
favor, veja o anúncio no
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">Misc. Developer News</a> e no
<a href="https://lists.debian.org/debian-devel/2020/02/msg00500.html">developers-reference translations</a>
para maiores informações.</p>

<p><b>Chamada para propostas de artes para o bullseye</b></p>

<p>Jonathan Carter fez uma <a
href="https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html">chamada oficial
para propostas de artes para o bullseye</a>.
Para obter os detalhes mais atualizados, consulte a <a
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">wiki</a>. 
Ao mesmo tempo, gostaríamos de agradecer a Alex Makas por fazer o <a
href="https://wiki.debian.org/DebianArt/Themes/futurePrototype">tema 
futurePrototype para o buster</a>. Se você gostaria, ou conhece alguém que gostaria,
de criar um desktop para ver e sentir, não deixe de enviar sua arte. O prazo
de submissão é 10 de outubro de 2020.</p>

<p><b>Pacotes que precisam de ajuda:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2020/09/msg00058.html"
        orphaned="1191"
        rfa="213" />

<p><b>Bugs para recém-chegados(as)</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
O Debian tem uma tag para bug chamada <q>newcomer</q>, utilizada para 
indicar que bugs são adequados para novos(as) colaboradores(as) usarem como
entrada para trabalhar em um pacote específico.
 
Atualmente existem <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">195</a>
bugs disponíveis sinalizados como <q>newcomer</q>.
</p>

<toc-add-entry name="code">Código, codificadores(as) e colaboradores(as)</toc-add-entry>
<p><b>Novos(as) mantenedores(as) de pacotes desde 1 de julho 2019</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Por favor, dêem as boas-vindas a: Marie Alexandre, Saira Hussain, laokz, Arthur Diniz, Daniel
Pimentel, Ricardo Fantin da Costa, Ivan Noleto, Joao Seckler, Andre Marcelo
Alvarenga, Felipe Amorim, Pedro Vaz de Mello de Medeiros, Marco Villegas,
Thiago Gomes Verissimo, Teemu Toivola, Giuliano Augusto Faulin Belinassi,
Miguel Figueiredo,  Stephan Lachnit, Nicola Di Lieto, Ximin Luo, Paul Grosu,
Thomas Ward, Ganael Laplanche, Aaron M. Ucko, Rodrigo Carvalho, Lukas
Puehringer, Markus Teich, Alexander Ponyatykh, Rob Savoury, Joaquin de Andres,
Georges Basile Stavracas Neto, Jian-Ding Chen (timchen119), Juan Picca,
Katerina, Matthew Fernandez, Shane McDonald, Eric Desrochers, Remi Duraffort,
Sakirnth Nagarasa, Ambady Anand S., Abhijith Sheheer, Helen Koike, Sven Hartge,
Priyanka Saggu, Sebastian Holtermann, Jamie Bliss, David Hart, James Tocknell,
Julien Schueller, Matt Hsiao, Jafar Al-Gharaibeh, Matthias Blümel, Dustin
Kirkland, Gao Xiang , Alberto Leiva Popper, Benjamin Hof, Antonio Russo, Jérôme
Lebleu, Ramon Fried, Evangelos Rigas, Adam Cecile, Martin Habovstiak, Lucas
Kanashiro, Alessandro Grassi, Estebanb, James Price, Cyril Richard, John Scott,
David Bürgin, Beowulf, Igor Petruk, Thomas Dettbarn, Vifly, Lajos Veres,
Andrzej Urbaniak, Phil Wyett, Christian Barcenas, Johannes Schilling, Josh
Steadmon, Sven Hesse, Gert Wollny, suman rajan, kokoye2007, Kei Okada, Jonathan
Tan, David Rodriguez, David Krauser, Norbert Schlia, Pranav Ballaney, Steve
Meliza, Fabian Grünbichler, Sao I Kuan, Will Thompson, Abraham Raji, Andre
Moreira Magalhaes, Lorenzo Puliti, Dmitry Baryshkov, Leon Marz, Ryan Pavlik,
William Desportes, Michael Elterman, Simon, Schmeisser, Fabrice Bauzac, Pierre
Gruet, Mattia Biondi, Taowa Munene-Tardif, Sepi Gair, Piper McCorkle, Alois
Micard, xiao sheng wen, Roman Ondráček, Abhinav Krishna C K, Konstantin Demin,
Pablo Mestre Drake, Harley Swick, Robin Gustafsson, Hamid Nassiby, Étienne
Mollier, Karthik, Ben Fiedler, Jair Reis, Jordi Sayol, Emily Shaffer, Fabio dos
Santos Mendes, Bruno Naibert de Campos, Junior Figueredo, Marcelo Vinicius
Campos Amedi, Tiago Henrique Vercosa de Lima, Deivite Huender Ribeiro Cardoso,
Guilherme de Paula Xavier Segundo, Celio Roberto Pereira, Tiago Rocha, Leandro
Ramos, Filipi Souza, Leonardo Santiago Sidon da Rocha, Asael da Silva Vaz,
Regis Fernandes Gontijo, Carlos Henrique Lima Melara, Alex Rosch de Faria, Luis
Paulo Linares, Jose Nicodemos Vitoriano de Oliveira, Marcio Demetrio Bacci,
Fabio Augusto De Muzio Tobich, Natan Mourao, Wilkens Lenon, Paulo Farias,
Leonardo Rodrigues Pereira, Elimar Henrique da Silva, Delci Silva Junior, Paulo
Henrique Hebling Correa, Gleisson Jesuino Joaquim Cardoso, Joao Paulo Lima de
Oliveira, Jesus Ali Rios, Jaitony de Sousa, Leonardo Santos, Aristo Chen,
Olivier Humbert, Roberto De Oliveira, Martyn Welch, Arun Kumar Pariyar, Vasyl
Gello, Antoine Latter, Ken VanDine, Francisco M Neto, Dhavan Vaidya, Raphael
Medaer, Evangelos Ribeiro Tzaras, Richard Hansen, Joachim Langenbach, Jason
Hedden, Boian Bonev, Kay Thriemer, Sławomir Wójcik, Lukas Märdian, Kevin Duan,
zhao feng, Leandro Cunha, Cocoa, J0J0 T, Johannes Tiefenbacher, Iain Parris,
Guobang Bi, Lasse Flygenring-Harrsen, Emanuel Krivoy, Qianqian Fang, Rafael
Onetta Araujo, Shruti Sridhar, Alvin Chen, Brian Murray, Nick Gasson.
</p>

<p><b>Novos(as) mantenedores(as) Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Por favor, dêem as boas-vindas a: Hans van Kranenburg, Scarlett Moore, Nikos Tsipinakis, Joan
Lledó, Baptiste Beauplat, Jianfeng Li, Denis Danilov, Joachim Falk, Thomas
Perret, William Grzybowski, Lars Tangvald, Alberto Molina Coballes, Emmanuel
Arias, Hsieh-Tseng Shen, Jamie Strandboge, Sven Geuer, Håvard Flaget Aasen,
Marco Trevisan, Dennis Braun, Stephane Neveu, Seunghun Han, Alexander Johan
Georg Kjäll, Friedrich Beckmann, Diego M. Rodriguez, Nilesh Patra, Hiroshi
Yokota, Shayan Doust, Chirayu Desai, Arnaud Ferraris, Francisco Vilmar Cardoso
Ruviaro, Patrick Franz, François Mazen, Kartik Kulkarni, Fritz Reichwald, Nick
Black, Octavio Alvarez.
</p>

<p><b>Novos(as) Desenvolvedores(as) Debian</b></p>
<p>
Por favor, dêem as boas-vindas a: Keng-Yu Lin, Judit Foglszinger, Teus Benschop,
Nick Morrott, Ondřej Kobližek, Clément Hermann, Gordon Ball,
Louis-Philippe Véronneau, Olek Wojnar, Sven Eckelmann, Utkarsh Gupta,
Robert Haist, Gard Spreemann, Jonathan Bustillos, Scott Talbert, Paride Legovini,
Ana Custura, Felix Lechner, Richard Laager, Thiago Andrade Marques, Vincent Prat,
Michael Robin Crusoe, Jordan Justen, Anuradha Weeraman, Bernelle Verster,
Gabriel F. T. Gomes, Kurt Kremitzki, Nicolas Mora, Birger Schacht and
Sudip Mukherjee.
</p>

<p><b>Colaboradores(as)</b></p>

## Visit the link below and pull the information manually.

<p>
1375 pessoas e 9 equipes estão atualmente listadas na página
<a href="https://contributors.debian.org/">contribuidores(as) Debian</a> em
2020.
</p>


<p><b>Pacotes novos e notáveis</b></p>

<p>
Uma amostra dos muitos pacotes <a href="https://packages.debian.org/unstable/main/newpkg">
adicionados ao repositório do Debian instável (unstable)</a> nas últimas semanas:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/bpftool">bpftool - Inspeção e simples manipulação de mapas e programas do BPF</a></li>
<li><a href="https://packages.debian.org/unstable/main/elpa-magit-forge">elpa-magit-forge - Trabalha com forjas Git apoiando-se no conforto do Magit</a></li>
<li><a href="https://packages.debian.org/unstable/main/libasmjit0">libasmjit0 - Um montador (assembler) JIT e AOT completo em C++ para x86/x64</a></li>
<li><a href="https://packages.debian.org/sid/libraritan-rpc-perl">libraritan-rpc-perl - Módulo Perl para a interface Raritan JSON-RPC</a></li>
<li><a href="https://packages.debian.org/unstable/main/python3-rows">python3-rows - Biblioteca para dados tabulares, não importa o formato</a></li>
</ul>


<p><b>Era uma vez Debian:</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>

<li>02/09/2007 <a href="https://lists.debian.org/debian-devel-announce/2007/09/msg00001.html">packages.debian.org atualizado</a></li>

<li>04/09/2006 <a href="https://lists.debian.org/debian-devel-announce/2006/09/msg00002.html">cdrkit (cdrtools fork) carregado</a></li>

<li>04/09/2009 <a href="https://lists.debian.org/debian-devel-announce/2009/09/msg00002.html">pacote grub agora baseado no GRUB2</a></li>

<li>06/09/1997 <a href="https://lists.debian.org/debian-devel-announce/1997/09/msg00000.html">implementado acompanhamento de severidade no BTS</a></li>

<li>08/09/2008 <a href="https://lists.debian.org/debian-devel-announce/2008/09/msg00001.html">arquitetura m68k movida para debian-ports.org</a></li>

</ul>


<toc-add-entry name="continuedpn">Quer continuar lendo o DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Assine ou cancele</a> sua inscrição da lista das Notícias do Debian</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Laura Arjona Reina, Jean-Pierre Giraud, Paulo Henrique de Lima Santana, Jean-Philippe Mengual, Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
