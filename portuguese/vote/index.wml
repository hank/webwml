#use wml::debian::template title="Informações sobre votações do Debian" BARETITLE="true" NOHEADER="true" NOHOMELINK="true"
#use wml::debian::votebar
#use wml::debian::translation-check translation="5127161ff176cab5734ac40fd7c6f6ccf12526f4"

<H1 class="title">Informações sobre votações do Debian</H1>

	<p>
	  O Projeto Debian tem um sistema de acompanhamento de
	  votações (DEbian VOTe EnginE [<a href="https://vote.debian.org/~secretary/devotee.git/">devotee</a>])
	  que exibe o estado das resoluções gerais em andamento e os resultados
	  das votações anteriores.
	</p>

	<p>
	  O estado das resoluções gerais em curso inclui a
	  proposta e a lista de apoios, todas as datas importantes e
	  as regras necessárias para aceitação. Claro que o estado também
	  incluirá um dos pontos a seguir:
	</p>
	<ul>
	   <li>Proposta - Aguardando por apoio.</li>
	   <li>Discussão - Período mínimo de duas semanas para discussões.</li>
	   <li>Votação - O processo de votação está acontecendo.</li>
	   <li>Fechado - A votação acabou.</li>
	</ul>
		
	<p>
	  Os resultados das votações anteriores (resoluções fechadas)
	  incluem o resultado como também uma lista de todas pessoas que
          votaram e quais foram seus votos. Eles também contêm o texto de
          cada voto enviado, a não ser que a votação tenha sido secreta.
	</p>

	<p>
	  O Debian usa o <a
	  href="https://en.wikipedia.org/wiki/Condorcet_method">método
	  Condorcet</a> para as eleições de líder do projeto (o artigo
	  da wikipedia é bem informativo). Resumidamente, o método Condorcet
          puro pode ser definido como segue:
	</p>
	  <blockquote>
	  <p>
	  Considere todas as possíveis disputas em pares entre candidatos(as).
	  O(A) vencedor(a) Condorcet, se houver, é o(a) candidato(a) que
          vencer cada um(a) dos(as) outros(as) candidatos(as) em
	  cada disputa direta com aquele(a) candidato(a).
	  </p>
	  </blockquote>
	<p>
	  O problema é que em eleições complexas, pode ocorrer uma
	  relação circular na qual A vence B, B vence C e C
	  vence A. A maioria das variações do método Condorcet usa vários modos
	  para resolver este empate. Veja o <a
	  href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">descartamento
	  sequencial Schwartz imune a clones</a> para detalhes. A variação do Debian
	  está explicada na <a href="$(HOME)/devel/constitution">constituição</a>,
	  especificamente em § A.6.
	</p>
	<p>
	  Para mais informações sobre como ler a matriz de confrontos,
	  que é publicada conforme o resultado dos votos, você pode olhar
	  este <a
	  HREF="https://en.wikipedia.org/wiki/Schwartz_method">exemplo</A>.
	</p>
