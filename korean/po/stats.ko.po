# stats
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Sangdo Jun <sebuls@gmail.com>, 2018, 2020.
# Changwoo Ryu <cwryu@debian.org>, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml stats\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-06-16 03:28+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "데비안 웹사이트 번역 통계"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "번역할 페이지 %d개."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "번역할 용량 %d 바이트."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "번역할 문자열 %d개."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "그른 번역 버전"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "이 번역은 업데이트가 너무 오래 늦어졌습니다"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "원본이 이 번역보다 새롭습니다"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "원본이 더 이상 없습니다"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "조회수 N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "조회"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "클릭하여 차이점 통계 데이터를 가져오세요"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "<transstatslink>로 만들어짐"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "번역 요약"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "번역 안 됨"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "업데이트 늦어짐"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "번역됨"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "최신에 맞게 업데이트"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "파일"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "바이트"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"주의: 이 페이지 목록은 인기 순서로 정렬되었습니다. 조회수를 보려면 페이지 이"
"름 위로 마우스를 올리십시오."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "업데이트 늦어진 번역"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "파일"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "차이"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "설명"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "차이점 통계"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "git 명령"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "로그"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "번역"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "관리자"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "상태"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "번역자"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "날짜"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "번역 안 된 일반 페이지"

# NOTE: 위와 동일, <table> summary attribute
#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "번역 안 된 일반 페이지"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "번역 안 된 뉴스 항목"

# NOTE: 위와 동일, <table> summary attribute
#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "번역 안 된 뉴스 항목"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "번역 안 된 컨설턴트/사용자 페이지"

# NOTE: 위와 동일, <table> summary attribute
#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "번역 안 된 컨설턴트/사용자 페이지"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "번역 안 된 국제 페이지"

# NOTE: 위와 동일, <table> summary attribute
#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "번역 안 된 국제 페이지"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "번역된 페이지 (최신에 맞게 업데이트)"

# NOTE: 위와 동일, <table> summary attribute
#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "번역한 서식 (PO 파일)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO 번역 통계"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "퍼지"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "번역 안 됨"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "전체"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "전체:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "번역된 웹 페이지"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "페이지 수 기준 번역 통계"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "언어"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "번역"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "번역된 웹 페이지 (크기)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "페이지 크기 기준 번역 통계"
