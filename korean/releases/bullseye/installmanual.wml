#use wml::debian::template title="데비안 bullseye -- 설치 안내" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="83895cd0b0913e07ec4171a51420513771cf1c64" maintainer="Sebul"

<if-stable-release release="stretch">
<p>This is a <strong>beta version</strong> of the Installation Guide for Debian
10, codename buster, which isn't released yet. The information
presented here might be outdated and/or incorrect because of changes to
the installer. You might be interested in the
<a href="../stretch/installmanual">Installation Guide for Debian
9, codename stretch</a>, which is the latest released version of
Debian; or in the <a href="https://d-i.debian.org/manual/">Development
version of the Installation Guide</a>, which is the most up-to-date version
of this document.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>This is a <strong>beta version</strong> of the Installation Guide for Debian
11, codename bullseye, which isn't released yet. The information
presented here might be outdated and/or incorrect because of changes to
the installer. You might be interested in the
<a href="../buster/installmanual">Installation Guide for Debian
10, codename buster</a>, which is the latest released version of
Debian; or in the <a href="https://d-i.debian.org/manual/">Development
version of the Installation Guide</a>, which is the most up-to-date version
of this document.</p>
</if-stable-release>

<p>설치 지침은 다운로드 가능한 파일과 함께 지원되는 각 아키텍처에서 사용 가능:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>브라우저의 지역화를 올바르게 설정하면 
위의 링크를 사용하여 올바른 HTML 버전을 자동으로 가져올 수 있습니다. &mdash; 
<a href="$(HOME)/intro/cn">content negotiation</a>을 참조하십시오. 
또는, 아래 표에서 원하는 정확한 아키텍처, 언어 및 형식을 선택하십시오.
</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>아키텍처</strong></th>
  <th align="left"><strong>형식</strong></th>
  <th align="left"><strong>언어</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>

