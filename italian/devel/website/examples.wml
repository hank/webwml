#use wml::debian::template title="Esempi"
#use wml::debian::translation-check translation="d1af73e44d054ea9925b972ae2cb196c0365a2bb" maintainer="Giuseppe Sacco"

<h3>Esempio di come cominciare una nuova traduzione</h3>

<p>Per questo esempio useremo la lingua francese

<pre>
   git pull
   cd webwml
   mkdir french
   cd french
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/french,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc po
   make -C po init-po
   git add po/Makefile po/*.fr.po
</pre>

<p>Edita il file <tt>.wmlrc</tt> e cambia:
<ul>
  <li>'-D CUR_LANG=English' in '-D CUR_LANG=French'
  <li>'-D CUR_ISO_LANG=en' in '-D CUR_ISO_LANG=fr'
  <li>'-D CUR_LOCALE=en_US' in '-D CUR_LOCALE=fr_FR'
  <li>'-D CHARSET=iso-8859-1' in maniera appropriata.<br>
	Il francese casualmente ha la stessa codifica dei caratteri
	dell'inglese quindi non serve modificarlo, ma è comunque
	possibile cambiarlo per altre lingue.
</ul>
 
<p>Edita Make.lang e cambia 'LANGUAGE := en' in 'LANGUAGE := fr'.
Nel caso tu stia traducendo in una lingua che usa i caratteri multi-byte
dovresti cambiare anche altre variabili in quel file, per maggiori
informazioni leggi ../Makefile.common e forse altri esempi funzionanti come la
traduzione cinese.

<p>Vai nella directory french/po e traduci tutto quello che è nei file PO.
Questo dovrebbe essere abbastanza facile.

<p> Sii sempre sicuro di aver copiato il Makefile dentro ad ogni directory che
hai intenzione di tradurre. Ciò è necessario poiché il programma <code>make</code> viene
utilizzato per convertire i file .wml in HTML e <code>make</code> usa i Makefile.

<p>Quando si finisce di aggiungere pagine e modificare i file, si deve eseguire
<pre>
   git commit -m "Inserire qui il messaggio di commit"
   git push
</pre>
dalla directory webwml. A questo punto si può cominciare a tradurre le pagine.

<h3>Esempio di traduzione di una pagina</h3>

<p>Per questo esempio useremo la traduzione francese del contratto sociale

<pre>
   cd webwml
   ./copypage.pl /english/social_contract.wml
   cd french
</pre>

<p>Questo aggiungerà automaticamente l'intestazione translation-check,
riferita al file inglese originale. Inoltre crea la directory di destinazione
e il Makefile, se necessari.</p>

<p>Modifica il file social_contract.wml traducendo il testo. Non tradurre alcun
link e non cambiarli in alcun modo. Se vuoi cambiare altro chiedilo nella
lista debian-www. Quando hai finito, esegui

<pre>
   cvs commit -m "short description of the changes you made" social_contract.wml
</pre>

<h3>Esempio su come aggiungere una nuova directory</h3>

<p>In questo esempio aggiungiamo la versione francese della directory intro/

<pre>
   cd webwml/french
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

Assicurati che la nuova directory abbia il Makefile e sia stata inserita
(tramite commit) nel CVS. Altrimenti il make darà un errore

<h3>Esempio di conflitto</h3>

 <p>Questo esempio mostra un commit che non va a buon fine perché la copia
 sul repository è stata modificata dopo l'ultimo <kbd>git pull</kbd>.</p>

 <p>Hai cambiato il file file foo.wml. Quindi:</p>

 <pre>
    git add foo.wml
    git commit -m "fixed a broken link"
    git push
 </pre>

 mostrerà:

 <pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
 </pre>

 <p>o qualcosa di simile :)
       <br />
       <br />
    Questo significa che le tue modifiche <strong>non</strong> sono state pubblicate
    sul repository git a causa di conflitti.
       <br />
    Dovrai controllare cosa sia andato storto, risolvere i conflitti e tentare di
    fare nuovamente un commit/push.</p>
