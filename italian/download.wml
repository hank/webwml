#use wml::debian::template title="Grazie per aver scaricato Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="Luca Monducci"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>State scaricando Debian <:=substr '<current_initial_release>', 0, 2:>,
nome in codice <em><current_release_name></em>, netinst, per
<: print $arches{'amd64'}; :>.</p>

<p>Nel caso in cui il download non si avviasse automaticamente, fare clic su <a
href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Codici di controllo:
<a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a>
<a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signature</a></p>



<div class="tip">
	<p><strong>Importante</strong>: si raccomanda di <a
	href="$(HOME)/CD/verify">verificare i codici di controllo del file
	scaricato</a>.</p>
</div>

<p>Le ISO dell'Installatore Debian sono immagini ibride, vuol dire che
possono essere scritte indistintamente su un supporto CD/DVD/BD oppure su
una <a href="https://www.debian.org/CD/faq/#write-usb">chiavetta
USB</a>.</p>

<h2 id="h2-1">Altri installatori</h2>

<p>Altri installatori e immagini come i sistemi live, gli installatori non
in-linea per i sistemi senza un connessione di rete, gli installatori per
le CPU di altre architetture o per istanze cloud, possono essere trovate su
<a href="$(HOME)/distrib/">Ottenere Debian</a>.</p>

<p>Degli installatori non ufficiali contenenti <a
href="https://wiki.debian.org/Firmware"><strong>firmware
non-libero</strong></a>, utili con alcune schede di rete e schede video,
possono essere scaricati da <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Immagini
non ufficiali che includono pacchetti con firmware non-libero</a>.</p>

<h2 id="h2-2">Collegamenti correlati</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Guida all'installazione</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Note di rilascio</a></p>
<p><a href="$(HOME)/CD/verify">Guida alla verifica dei file ISO</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Siti alternativi da cui scaricare</a></p>
<p><a href="$(HOME)/releases">Altri rilasci</a></p>
