msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2008-08-28 00:43+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian for Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian for PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "Hurd CD-er"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian for IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Kontakt"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "Prosessorer"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Takk"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Utvikling"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Dokumentasjon"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Installasjon"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr ""

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Pekere"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Nyheter"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Tilpasning"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Tilpasninger"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Problemer"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Programvarekatalog"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Status"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Tilgjengelighet"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Systemer"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD for i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD for Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Hvorfor"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Folk"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian for PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian for Sparc"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian for bærbare"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian for AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian for ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian for Beowulf"

#~ msgid "Main"
#~ msgstr "Hoved"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian for Motorola 680x0"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian for MIPS"

#~ msgid "Debian for S/390"
#~ msgstr "Debian for S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian for Sparc64"
