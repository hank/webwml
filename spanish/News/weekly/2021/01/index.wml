#use wml::debian::projectnews::header PUBDATE="2021-03-18" SUMMARY="Le damos la bienvenida a las DPN, DPL, Congelación débil, Protección de GRUB2, Tema Homeworld, Fallos RC, Actualización del sitio web, APT2.2, QEMU, tipos de medio, rust-coreutils, debuginfod, Google y Debian, Financiación por parte de Freexian, Nuevas claves de firma, BSP, y Contribuidores."
#use wml::debian::acronyms
#use wml::debian::translation-check translation="dc6b8f3e048c8f288c5efd1d612b75bbc4e51406"

# Status: [published]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<shortintro issue="primera"/>

<h2>Elecciones a líder del proyecto Debian 2021</h2>

<p>Se ha anunciado la <a href="https://lists.debian.org/debian-devel-announce/2021/03/msg00001.html"> 
apertura del plazo para presentación de candidaturas</a> con vistas a la elección
de <a href="https://www.debian.org/devel/leader">líder del proyecto Debian</a>. 
La elección se inicia seis semanas antes de que el
puesto quede vacante, empezando el nuevo mandato el 21 de abril de 2021. El proceso se desarrolla de la siguiente manera:
aceptación de candidaturas entre el 7 y el 13 de marzo; campaña entre el domingo,
14 de marzo, y el 3 de abril; y votación entre el 4 y el 17 de abril.</p>


<h2>Congelación débil de Bullseye</h2>

<p>
El equipo responsable de la publicación anunció que <a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00002.html
">bullseye entró en la fase de congelación débil el 12 de febrero de 2021</a>. Esta congelación
solo permite pequeñas modificaciones destinadas a corregir errores que afecten a la próxima publicación. Ya no se permiten
transiciones nuevas ni versiones de paquetes que puedan ser disruptivas. Puede
seguir el calendario de congelaciones en la página 
<a href="https://release.debian.org/bullseye/freeze_policy.html">Bullseye Freeze Timeline and Policy («Normas y cronograma de las congelaciones de Bullseye»)</a>.
</p>

<introtoc/>

<toc-display/>

<toc-add-entry name="security">Avisos de seguridad de Debian importantes</toc-add-entry>

<p>El equipo de seguridad de Debian publica diariamente avisos
(<a href="$(HOME)/security/2021/">Avisos de seguridad de 2021</a>).
Léalos con atención y suscríbase a la <a href="https://lists.debian.org/debian-security-announce/">lista de correo de
seguridad</a> para mantener sus sistemas actualizados frente a cualquier vulnerabilidad.</p>

## Pull the below data directly from $(HOME)/security/2021/. Swap out the 
## dsa-XXXX for the current advisory # , be sure to change the name as well. 

<p>Algunos avisos publicados recientemente se refieren a estos paquetes:
<a href="$(HOME)/security/2021/dsa-4870">pygments</a>,
<a href="$(HOME)/security/2021/dsa-4869">tiff</a>,
<a href="$(HOME)/security/2021/dsa-4868">flatpack</a>,
<a href="$(HOME)/security/2021/dsa-4867">grub2</a>,
<a href="$(HOME)/security/2021/dsa-4866">thunderbird</a>,
<a href="$(HOME)/security/2021/dsa-4865">docker.io</a>,
<a href="$(HOME)/security/2021/dsa-4862">firefox</a>,
<a href="$(HOME)/security/2021/dsa-4861">screen</a>,
<a href="$(HOME)/security/2021/dsa-4858">chromium</a>,
<a href="$(HOME)/security/2021/dsa-4854">webkit2gtk</a>,
<a href="$(HOME)/security/2021/dsa-4852">openvswitch</a>,
<a href="$(HOME)/security/2021/dsa-4849">firejail</a> y
<a href="$(HOME)/security/2021/dsa-4847">connman</a>.
</p>

<p>El sitio web de Debian <a href="https://www.debian.org/lts/security/">archiva</a> también
los avisos de seguridad publicados por el equipo de soporte a largo plazo de Debian (LTS, por sus siglas en inglés) y enviados a la
<a href="https://lists.debian.org/debian-lts-announce/">lista de correo debian-lts-announce</a>.
</p>

<toc-add-entry name="secureboot">Protección del arranque seguro UEFI con GRUB2 2021</toc-add-entry>

<p>Desde el conjunto
de fallos <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">BootHole</a>
en GRUB2 anunciado en julio de 2020, investigadores de seguridad y
desarrolladores de Debian y de otros proyectos han seguido buscando problemas
que pudieran permitir la elusión del arranque seguro UEFI. Se han encontrado
varios. Vea el <a href="https://www.debian.org/security/2021/dsa-4867">aviso de seguridad de Debian 4867-1</a>
para más detalles. Debian publicó una <a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot/">declaración</a> informativa
con el propósito de explicar las consecuencias de estas vulnerabilidades de seguridad y los
pasos que se han seguido para abordarlas.
</p>

<toc-add-entry name="bullseye">Noticias sobre Debian <q>bullseye</q></toc-add-entry>
## The first sub section is dedicated to news about bullseye in order to 
## build anticipation for our $Next_Release. 

## As we get closer to the release date, this section will home the 
## Release-Critical bugs report. 

<p><b>Homeworld, la obra artística y tema por omisión para <q>bullseye</q></b></p>
<p>
Enviamos nuestras felicitaciones a Juliette Taka por el envío de la obra ganadora
<a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Homeworld</a>, 
que será el tema y la obra artística por omisión de Debian 11 <q>bullseye</q>. Se emitieron
más de 5613 votos a <a 
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">18
candidaturas</a>. Gracias a quienes contribuyeron y a quienes votaron en este proceso, que
fue un delicioso recordatorio de la gran comunidad que en verdad es Debian.
</p>

<p>Informe de fallos críticos para la publicación en la semana 11</p>

<p>La <a href="https://udd.debian.org/bugs.cgi">interfaz web de fallos
de la base de datos Debian Ultimate</a>
tiene información, en la actualidad, de los siguientes fallos críticos para la publicación:</p>
<table>
  <tr><th>En total:</th><td><a href="https://udd.debian.org/bugs.cgi?release=any&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1110</a></td></tr>
  <tr><th>Que afectan a Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">216</a></td></tr>
  <tr><th>Solo Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_not_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">51</a></td></tr>
  <tr><th>Resultantes a corregir en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>165</b></a></td></tr>
</table>

<p>En estos <b><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">165</a></b> fallos están marcadas las etiquetas de clasificación siguientes:</p>

<table>
  <tr><th>Pendientes en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=only&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Con parche en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=only&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">23</a></td></tr>
  <tr><th>Duplicados en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=only&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">10</a></td></tr>
  <tr><th>Pueden corregirse en una actualización de seguridad:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=only&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">15</a></td></tr>
  <tr><th>Contrib o non-free en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=only&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>«Claimed» (alguien se ha ofrecido para corregirlos) en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=only&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">0</a></td></tr>
  <tr><th>Retrasados en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=only&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1</a></td></tr>
  <tr><th>Otros casos corregidos en Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=only&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">8</a></td></tr>
</table>

<p>Ignorando todos los anteriores (múltiples etiquetas posibles), es necesario que los y las contribuidoras a Debian corrijan <a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=ign&amp;pending=ign&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=ign&amp;deferred=ign&amp;notmain=ign&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=ign&amp;done=ign&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>111</b></a>
fallos para que se publique Debian 11.0 <q>bullseye</q>.</p>

<p>Sin embargo, desde el punto de vista de los responsables de la publicación («Release Managers»),
para que tenga lugar la publicación es necesario resolver
<a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=ign&amp;notbullseye=ign&amp;base=&amp;standard=&amp;merged=ign&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>182</b></a>.</p>

<p>Consulte la <q><a
href="https://wiki.debian.org/ProjectNews/RC-Stats">"Interpreting the release critical bug statistics" ("Interpretación de las
estadísticas de fallos críticos para la publicación")</a></q> para una explicación de las cifras mostradas.
</p>

<toc-add-entry name="web">Nuevo diseño del sitio web</toc-add-entry>

<p>Estamos muy orgullosos y orgullosas de nuestro equipo de WWW y de su trabajo en la <a href="https://bits.debian.org/2020/12/debian-web-new-homepage.html">transformación</a> del 
 <a href="https://www.debian.org">sitio web de Debian</a>, al que ha dado un aspecto más
moderno. ¡Échele un vistazo! Este es solo el comienzo de un proceso en el que seguiremos eliminando información
obsoleta, actualizaremos el sitio con información nueva y mejoraremos la experiencia
global de los usuarios y usuarias. Puesto que siempre resulta beneficioso contar con más manos y con más ojos, háganos saber si
desearía contribuir a este nuevo capítulo de nuestro desarrollo.</p>

<toc-add-entry name="apt">apt-2.2</toc-add-entry>

<p>Julian Andres Klode <a href="https://blog.jak-linux.org/2021/02/18/apt-2.2/">contó</a> que se ha
publicado APT 2.2. Sus nuevas funcionalidades incluyen <i>--error-on=any</i> y <i>rred</i>. Este último
método se puede utilizar como un programa independiente para fusionar ficheros pdiff.</p>


<toc-add-entry name="keys">Nuevas claves de firma del archivo</toc-add-entry>

<p>Se han generado para uso futuro (en breve) las nuevas claves de firma del archivo para
Debian 11. Se incluirán en Debian 11, <q>bullseye</q>, y en las futuras
versiones de Debian 10, <q>buster</q>. Estas claves empezarán a utilizarse cuando se publique
bullseye o cuando expiren las claves actuales, el 12 de abril de 2027.</p>

Las nuevas claves son:
<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>1F89 983E 0081 FDE0 18F3  CC96 73A4 F27B 8DD4 7936</th></tr>
<tr><th>uid</th>   <th>Debian Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th> rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>A723 6886 F3CC CAAD 148A  27F8 0E98 404D 386F A1D9</th></tr>
</table>

<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>AC53 0D52 0F2F 3269 F5E9  8313 A484 4904 4AAD 5C5D</th></tr>
<tr><th>uid</th>   <th>Debian Security Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th>rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>ED54 1312 A33F 1128 F10B  1C6C 5440 4762 BBB6 E853</th></tr>
</table>

<p>
<b>Claves:</b>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11.asc">https://ftp-master.debian.org/keys/archive-key-11.asc</a></p>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11-security.asc">https://ftp-master.debian.org/keys/archive-key-11-security.asc</a></p>

<toc-add-entry name="dqib">Disponibles imágenes precocinadas con Debian Quick Image Baker</toc-add-entry>
<p>DQIB (Debian Quick Image Baker) proporciona
<a href="https://people.debian.org/~gio/dqib/">imágenes de Debian sid para QEMU</a> generadas semanalmente para muchas arquitecturas. Cada descarga proporciona un sistema de ficheros
raíz, núcleo, initrd y un README con una orden QEMU de ejemplo que lanzará
la imagen y que contiene información sobre cómo acceder al sistema («log in»).

<toc-add-entry name="other">Otras cuestiones de interés</toc-add-entry> 
<ul>
<li>Charles Plessy <a href="https://lists.debian.org/debian-devel/2021/01/msg00057.html">añadió 
cientos de tipos de medio a /etc/mime.types</a>.</li>
<li>Nueva lista de correo: <a href="https://lists.debian.org/debian-localgroups/">debian-localgroups@lists.debian.org</a>, puesta en marcha para facilitar la comunicación
y proporcionar apoyo para eventos y actividades locales.</li>
</ul>

<toc-add-entry name="rust">Ejecución de Debian con Rust coreutils</toc-add-entry>
<p>Sylvestre Ledru <a href="https://sylvestre.ledru.info/blog/2021/03/09/debian-running-on-rust- 
coreutils">compartió detalles</a> sobre el funcionamiento de <a href="https://tracker.debian.org/pkg/rust-coreutils">rust-coreutils</a> en
Debian. La implementación soporta el arranque con GNOME, la instalación de los
principales 1000 paquetes y la compilación de Firefox.
</p>

<toc-add-entry name="debuginfod">Nuevo servicio: debuginfod</toc-add-entry>
<p>Sergio Durigan Junior anunció un <a href="https://wiki.debian.org/Debuginfod">servicio debuginfod en Debian</a>. <a 
href="https://sourceware.org/elfutils/Debuginfod.html">debuginfod</a> libera 
a los desarrolladores de la necesidad de instalar paquetes
con información de depuración para depurar software. Funciona en modo cliente/servidor para proporcionar
herramientas de depuración vía HTTP.
</p>




####Here is where we move to EXTERNAL NEWS about Debian####
## News about Debian, a Debian influence in F/OSS, F/OSS, mentions of Debian 
## in the news or other media.
<toc-add-entry name="bazel">Google y Debian colaboran para empaquetar el
sistema de compilación Bazel en Debian</toc-add-entry>

<p>
Olek Wojnar, desarrollador de Debian, y Yun Peng, ingeniero de software en Google, trabajaron con
el <a href="https://blog.bazel.build/2021/03/04/bazel-debian-packaging.html">equipo
de Bazel</a> para empaquetar <a href="https://opensourcelive.withgoogle.com/events/bazel/register?after-register
=%2Fevents%2Fbazel%2Fwatch%3Ftalk%3Ddebian-ubuntu-packaging">Bazel en 
Debian</a> con el objetivo de ayudar a la comunidad médica en la investigación de la COVID-19. Olek 
comparte algunos de los desafíos técnicos y de los detalles del proyecto en una
<a href="https://www.youtube.com/watch?v=jLSgky4ISj0&amp;t=23s">presentación en vídeo.</a><p>


<toc-add-entry name="freexian">Freexian proporcionará financiación para algunos proyectos de Debian</toc-add-entry>

<p>
Raphael Hertzog compartió detalles sobre la intención del soporte a largo plazo (LTS, por sus siglas en inglés) de Freexian de
<a href="https://lists.debian.org/debian-project/2020/11/msg00002.html">proporcionar financiación</a> 
para algunos proyectos de Debian con una parte del dinero reunido procedente
de sus propios patrocinadores. Esta generosa contribución permitirá que lo equipos propongan
solicitudes de financiación dentro de sus entornos de actividad lo que, en última instancia, reportará beneficios
al conjunto de la comunidad.
</p>

<toc-add-entry name="events">BSP, eventos, miniDebCamps y miniDebConfs</toc-add-entry>

<p><b>Fiestas caza-fallos</b></p>

<p><b>Próximos eventos</b></p>

<p>
Se celebrará una <a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00000.html">fiesta caza-fallos virtual en Salzburgo, Austria</a> entre el 24 y el 25 de abril de 2021.
Algunos detalles están todavía en fase de planificación; por el momento, por favor, reserve las fechas.</p>

<p><b>Eventos pasados</b></p>

<p>
La comunidad de Debian de Brasil celebró la <a href="https://mdcobr2020.debian.net/">miniDebConf en línea Brasil 2020</a>  
entre el 28 y el 29 de noviembre de 2020. Las charlas del evento, en portugués, están disponibles para su
<a href="https://peertube.debian.social/videos/watch/playlist/875b2409-2d73-4993- 
8471-2923c27b8a7e">visualización.</a>
</p>

<p>La comunidad de Debian de la India celebró la <a href="https://in2021.mini.debconf.org/">miniDebConf en línea India 2021</a> 
entre el 23 y el 24 de enero de 2021. Hubo charlas en más de seis idiomas, con alrededor de
45 eventos en total entre charlas, reuniones informales (BOF, por sus siglas en inglés: «Birds of a Feather») y talleres. Hay disponibles para su visualización charlas y <a 
href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-
India/">vídeos</a> del evento.
</p>


<toc-add-entry name="reports">Informes</toc-add-entry>

## Team and progress reports. Consider placing reports from teams here rather 
## than inside of the internal news sections. 



## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#


<p><b>Informes mensuales de Freexian sobre LTS</b></p>

<p>Freexian publica <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">informes mensuales</a>
sobre el trabajo de los contribuidores remunerados para el soporte a largo plazo de Debian.
</p>

<p><b>Estado actual de Compilaciones reproducibles</b></p>

<p>Siga en el <a
href="https://reproducible-builds.org/blog/">blog de
Compilaciones reproducibles</a> los informes mensuales de su trabajo en el ciclo de vida de <q>Buster</q>.
</p>


<p><b>Paquetes que necesitan ayuda</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2021/03/msg00063.html"
        orphaned="1204"
        rfa="209" />

<p><b>Fallos para principiantes</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian tiene una etiqueta de clasificación de fallos llamada <q>newcomer</q> («principiante»), usada para marcar fallos adecuados para que los nuevos
contribuidores los utilicen como punto de entrada para trabajar en paquetes específicos.

En la actualidad hay <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">189</a>
fallos disponibles marcados <q>newcomer</q>.
</p>


<toc-add-entry name="code">Programas, programadores y contribuidores</toc-add-entry>
<p><b>Nuevos y nuevas responsables de paquetes desde el 9 de septiembre de 2020</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Damos la bienvenida a: Adrien Nayrat, Georgy Komarov, Alex Doyle, Johann Queuniet,
Stephen Gildea, Christoph Groth, Jhon Alejandro Marin Rodriguez, Adrià
García-Alzórriz, Romain Porte, Jakub Ružička, skyper, James Turton, Alois
Schlögl, Judit Foglszinger, Aaron Boxer, Kevin Wu, Anthony Perkins, Felix
Delattre, Ken Ibbotson, Andrei Rozanski, Nis Martensen , qinxialei, Laurin
Hagemann, Jai Flack, Johann Elsass, Fred Le Meur, Vivek K J, Thiago da Silva
Gracini, Jobin J, Selvamani Kannan, Calum McConnell, Dhyey Patel, Ed Neville,
Leonidas S. Barbosa, Lucca Braga Godoy Mendonça, Chris Keller, Guinness,
Sergio de Almeida Cipriano Junior, Sahil Dhiman, Michel Le Bihan, Fabio
Fantoni, Mark Pearson, Matija Nalis, David Bannon, Federico Grau, Lisa Julia
Nebel, Patrick Jaap, Francisco Emanoel Ferreira, Peymaneh Nejad, Daniel Milde,
Stefan Kropp, Frédéric Pierret, Vipul Kumar, Jarrah Gosbell, John Zaitseff,
Badreddin Aboubakr, Sam Reed, Scupake, Clay Stan, Klaumi Klingsporn, Vincent
Smeets, Emerson dos Santos Queiroz, Alexander Sulfrian, bill-auger, Marcelo
Henrique Cerri, Dan Streetman, Hu Feng, Andrea Righi, Matthias Klein, Eric
Brown, Mayco Souza Berghetti, Robbi Nespu, Simon Tatham y Brian Potkin.
</p>

<p><b>Nuevos mantenedores y mantenedoras de Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Damos la bienvenida a: Ricardo Ribalda Delgado, Pierre Gruet, Henry-Nicolas Tourneur,
Aloïs Micard, Jérôme Lebleu, Nis Martensen, Stephan Lachnit, Felix Salfelder,
Aleksey Kravchenko, Étienne Mollier, Timo Röhling, Fabio Augusto De Muzio
Tobich, Arun Kumar Pariyar, Francis Murtagh, William Desportes, Robin
Gustafsson, Nicholas Guriev, Xiang Gao, Maarten L. Hekkelman, qinxialei,
Boian Bonev, Filip Hroch y Antonio Valentino.
</p>

<p><b>Nuevos desarrolladores y desarrolladoras de Debian</b></p>
<p>
Damos la bienvenida a: Benda XU, Joseph Nahmias, Marcos Fouces, Hayashi Kentaro,
James Valleroy, Helge Deller, Nicholas D Steeves, Nilesh Patra, David Suárez
Rodríguez y Pierre Gruet.
</p>
<p><b>Contribuidores y contribuidoras</b></p>
## Visit the link below and pull the information manually.

<p>
En la página de <a href="https://contributors.debian.org/">contribuidores y contribuidoras a Debian</a> de
2021 hay 954 personas y 9 equipos listados en la actualidad.
</p>

<p><b>Estadísticas</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>buster</em></b></p>
<ul style="list-style-type:none">
<li>Ficheros fuente: 12&nbsp;323&nbsp;884</li>
<li>Paquetes fuente: 28&nbsp;925</li>
<li>Disco usado: 264&nbsp;071&nbsp;084 kB</li>
<li>Ctags: 9&nbsp;487&nbsp;034</li>
<li>Líneas de código fuente: 1&nbsp;077&nbsp;110&nbsp;982</li>
</ul>

<p><b><em>sid</em></b></p>
<ul style="list-style-type:none">
<li>Ficheros fuente: 16&nbsp;868&nbsp;320</li>
<li>Paquetes fuente: 33&nbsp;215</li>
<li>Disco usado: 364&nbsp;735&nbsp;804 kB</li>
<li>Ctags: 3&nbsp;343&nbsp;666</li>
<li>Líneas de código fuente: 1&nbsp;510&nbsp;195&nbsp;519</li>
</ul>

<p><b>Paquetes populares</b></p>
##Taken via UDD query from popcon
<ul>
<li><a href="https://packages.debian.org/buster/tar">tar</a>: 99&nbsp;100 usuarios diarios</li>
<li><a href="https://packages.debian.org/buster/popularity-contest">popularity-contest</a>: 96&nbsp;010 usuarios diarios</li>
<li><a href="https://packages.debian.org/buster/debianutils">debianutils</a>: 112&nbsp;659 instalaciones</li>
<li><a href="https://packages.debian.org/buster/gparted">gparted</a>: 14&nbsp;909 instalaciones</li>
<li><a href="https://packages.debian.org/buster/grub2-common">grub2-common</a> 54&nbsp;040 actualizaciones recientes</li>
</ul>

<p><b>Paquetes nuevos y de interés</b></p>

<p>
Aquí hay una pequeña muestra de los muchos paquetes <a href="https://packages.debian.org/unstable/main/newpkg">
añadidos al archivo de Debian «inestable»</a> en las últimas semanas:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/dnsperf">dnsperf -Herramienta para prueba del rendimiento del DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/ptpython">ptpython -Interfaz alternativa del intérprete de Python con autocompletado</a></li>
<li><a href="https://packages.debian.org/unstable/main/science-datamanagement">science-datamanagement -Paquetes Debian para gestión de datos científicos</a></li>
<li><a href="https://packages.debian.org/unstable/main/dnscap">dnscap -Utilidad para captura de red diseñada específicamente para tráfico DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/logiops">logiops -Utilidad para configuración de ratones y teclados Logitech</a></li>
<li><a href="https://packages.debian.org/unstable/main/node-cron-validator">node-cron-validator -- cron-validator es una utilidad que permite validar expresiones de cron</a></li>
</ul>

<toc-add-entry name="discuss">Debates</toc-add-entry>

<p>Stephan Lachnit, desarrollador de Debian, preguntó: ¿<a href="https://lists.debian.org/debian-devel/2021/02/msg00282.html"><i>Es
posible contribuir a Debian sin utilizar el nombre real por razones de privacidad</i></a>?</p>


<p>William Torrez Corea preguntó: ¿<a href="https://lists.debian.org/debian-user/2021/01/msg00321.html"><i>Cómo
actualizo el sistema operativo a Debian buster</i></a>? El hilo contempla
reinstalación vs. actualización vs. recompilación y una sinopsis de Sid/«estable»/«en pruebas».
</p>


<p>Jerry Mellon preguntó: ¿<a  href="https://lists.debian.org/debian-user/2021/01/msg00584.html"><i>Cómo añadir
un disco duro a un sistema existente</i></a>? Fácil lectura que muestra algunos obstáculos
y posibles soluciones para esta habitual tarea.
</p>

<p>Dan Hitt preguntó: ¿<a href="https://lists.debian.org/debian-user/2021/01/msg00746.html"><i>Cómo instalar
Debian 10 sin acceso a CD ni a USB pero con ethernet y disco duro
en uso</i></a>? En esta discusión se habla de una BIOS muy rápida, de opciones de pxeboot, de entradas
de menú de Grub, de netboot y de una solución de núcleo hd-media.</p>

<p>John Berden preguntó: ¿<a href="https://lists.debian.org/debian-user/2021/02/msg00164.html"><i>Cómo corregir
una contraseña incorrecta en Debian 10.8 después de la instalación</i></a>?. Este hilo
toca brevemente la edición de grub durante el arranque, la persistencia en grub, una lección en miniatura de 
sintaxis de emacs y de grub y el comportamiento canónico del intérprete de órdenes («shell»).</p>

<p><b>Trucos y consejos</b></p>

<ul>
<li>Craig Small explica el <a href="https://dropbear.xyz/2021/01/18/percent-cpu-for-processes/">campo 
CPU/pcpu del programa ps</a>, que la mayoría ejecutamos como ps aux. Es una gran
explicación de cómo se realizan los cálculos y se presentan los resultados.</li>

<li>Vincent Fourmond comparte todos sus <a href="https://vince-debian.blogspot.com/2021/03/all-tips-and-tricks-about-qsoas.
html">trucos y consejos sobre QSoas</a>.</li>

<li>Bastian Venthur detalla la <a href="https://venthur.de/2021-02-10-installing-debian-on-a-thinkpad-t14s.
html">instalación de Debian en un Thinkpad T14s</a>.</li>
</ul>

<p><b>Érase una vez en Debian</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>
<li>2010-03-13 a 2010-03-19 <a href="https://wiki.debian.org/DebianThailand/MiniDebCamp2010">MiniDebCamp 2010 en Tailandia</a></li>

<li>2014-03-15 a 2014-03-16 <a href="https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/">MiniDebConf de mujeres, Barcelona, España</a></li>

<li>2000-03-16 <a href="https://www.debian.org/vote/2000/vote_0007">Wichert Akkerman reelegido DPL</a></li>

<li>1997-03-17 <a href="https://www.debian.org/News/1997/19970317">Ian Murdock elegido presidente de la Junta de Dirección</a></li>

<li>2005-03-17 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=300000">Florian Zumbiehl informa del fallo #300000 de Debian</a></li>

<li>2008-03-17 <a href="https://www.debian.org/devel/debian-installer/News/2008/20080317">Publicada la primera versión beta del instalador Debian de lenny</a></li>
</ul>

<toc-add-entry name="continuedpn">¿Quiere seguir leyendo DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Suscríbase o cancele su suscripción</a> a la lista de correo de Noticias Debian</p>

#use wml::debian::projectnews::footer editor="el equipo de publicidad («Publicity Team») con contribuciones de Jean-Pierre Giraud, Justin B Rye, Thiago Pezzo, Paulo Santana, Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
