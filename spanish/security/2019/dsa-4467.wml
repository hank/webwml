#use wml::debian::translation-check translation="a0d25d78b2c207b5e643fbc99f4957ec35c99fda"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>El usuario <q>Arminius</q> descubrió una vulnerabilidad en Vim, una versión mejorada del
editor UNIX estándar Vi («Vi IMproved», o Vi mejorado). El proyecto «Vulnerabilidades y exposiciones comunes» («Common Vulnerabilities
and Exposures») identifica el problema siguiente:</p>

<p>Es habitual que los editores proporcionen una manera de incorporar órdenes de configuración del editor (también llamadas
«modelines») que se ejecutan al abrir un fichero, al tiempo que las órdenes potencialmente nocivas
se filtran mediante un mecanismo de entorno aislado («sandbox»). Se descubrió que la orden
<q>source</q> (usada para incluir y ejecutar otro fichero) no se filtraba, lo que permitía
la ejecución de órdenes del intérprete de órdenes («shell») mediante la apertura con Vim de un fichero cuidadosamente preparado.</p>

<p>Para la distribución «estable» (stretch), este problema se ha corregido en
la versión 2:8.0.0197-4+deb9u2.</p>

<p>Le recomendamos que actualice los paquetes de vim.</p>

<p>Para información detallada sobre el estado de seguridad de vim, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4467.data"
