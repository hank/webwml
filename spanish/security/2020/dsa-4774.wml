#use wml::debian::translation-check translation="43f8d7b8b91b167696b5c84ec0911bab7b7073f2"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a ejecución de código arbitrario, a elevación de privilegios,
a denegación de servicio o a fugas de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

    <p>Andy Nguyen descubrió un defecto en la manera en que son gestionados los paquetes
    L2CAP con A2MP CID en la implementación de Bluetooth. Un atacante remoto a
    corta distancia que conozca la dirección del dispositivo Bluetooth de la víctima puede
    enviar un paquete l2cap malicioso y provocar denegación de servicio o,
    posiblemente, ejecución de código arbitrario con privilegios de kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

    <p>Andy Nguyen descubrió un defecto en la implementación de Bluetooth. La pila
    no se inicializa correctamente al tratar determinados paquetes
    AMP. Un atacante remoto a corta distancia que conozca la dirección
    del dispositivo Bluetooth de la víctima puede obtener información de la pila del núcleo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

    <p>Se descubrió un defecto en el subsistema netfilter. Un atacante local
    con capacidad para inyectar configuración de conntrack Netlink puede provocar denegación
    de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

    <p>ChenNan, del Chaitin Security Research Lab, descubrió un defecto en el
    módulo hdlc_ppp. Una validación incorrecta de la entrada en la función
    ppp_cp_parse_cr() puede dar lugar a corrupción de memoria y a revelación de información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

    <p>Se descubrió un defecto en el controlador de interfaz para tráfico
    encapsulado GENEVE cuando se combina con IPsec. Si IPsec está
    configurado para cifrar el tráfico del puerto UDP usado por el
    túnel GENEVE, los datos enviados por el túnel no son correctamente enrutados por la
    conexión cifrada y, en su lugar, son enviados en claro.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.152-1. Las vulnerabilidades están corregidas mediante la aplicación de las modificaciones («rebasing») a la nueva
versión «estable» 4.19.152 del proyecto original, que incluye, además, correcciones para otros fallos.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4774.data"
