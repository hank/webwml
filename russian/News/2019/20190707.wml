#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794" maintainer="Lev Lamberov"
<define-tag pagetitle>Debian Edu / Skolelinux Buster &mdash; полное Linux-решение для вашей школы</define-tag>
<define-tag release_date>2019-07-07</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
Вам приходится администрировать компьютерную лабораторию или всю школьную сеть?
Вам хотелось быть установить серверы, рабочие станции и ноутбуки, которые бы затем
работали совместно?
Вам нужны стабильность Debian и предварительно настроенные сетевые
службы?
Вы хотите иметь веб-инструмент для управления системами и несколькими
сотнями или даже большим числом учётных записей пользователей?
Спрашивали ли вы себя о том, могут ли и как могут использоваться для этого более старые компьютеры?
</p>

<p>
Тогда Debian Edu для вас. Сами учителя или же сотрудники технической поддержки
могут установить полное многопользовательское обучающее окружение из нескольких компьютеров
всего за пару дней. Debian Edu поставляется с сотнями заранее настроенных приложений,
но вы всегда можете добавить дополнительные пакеты из Debian.
</p>

<p>
Команда разработки Debian Edu с радостью объявляет о выпуске Debian Edu 10
<q>Buster</q>, системы Debian Edu / Skolelinux на основе
Debian 10 <q>Buster</q>.
Попробуйте его и свяжитесь с нами (&lt;debian-edu@lists.debian.org&gt;),
это поможет нам сделать его ещё лучше.
</p>

<h2>О Debian Edu и Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu">Debian Edu, также известный как
Skolelinux</a>, является дистрибутивом Linux на основе Debian, он из коробки
предоставляет полностью настроенное окружение для школьной сети.
Сразу же после установки школьный сервер запускает все службы,
которые требуются для школьной сети, и ожидает добавления пользователей и
машин через комфортный веб-интерфейс GOsa².
Подготавливается окружение сетевой загрузки, поэтому после изначальной
установки основного сервера с компакт-диска / DVD / BD или USB-носителя все
остальные машины можно установить по сети.
Более старые компьютеры (даже те, которым десять или около того лет) могут использоваться
в качестве тонких LTSP-клиентов или бездисковых рабочих станций, загрузка которых
будет производиться по сети вообще без установки и настройки.
Школьный сервер Debian Edu предоставляет базу данных LDAP и службу аутентификации
Kerberos, централизованные домашние каталоги, DHCP-сервер, веб-прокси
и множество других служб. Система для настольных компьютеров содержит более 60 пакетов с
образовательным ПО, а ещё большее их число доступно из архива Debian.
Школы могут выбрать любое окружение рабочего стола, доступны Xfce, GNOME, LXDE,
MATE, KDE Plasma и LXQt.
</p>

<h2>Новые возможности Debian Edu 10 <q>Buster</q></h2>

<p>Ниже приводятся некоторые пункты из информации о выпуске Debian Edu 10 <q>Buster</q>,
на основе Debian 10 <q>Buster</q>.
Полный список, включающий в себя более подробную информацию, является частью соответствующей
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">главы руководства по Debian Edu</a>.
</p>

<ul>
<li>
В настоящее время доступны официальные установочные образы Debian.
</li>
<li>
Возможна модульная установка для нужд конкретного учреждения.
</li>
<li>
Предоставляются дополнительные метапакеты, группирующие образовательные пакеты
по уровню школы.
</li>
<li>
Улучшена локализация приложений для всех поддерживаемых Debian языков.
</li>
<li>
Доступен инструмент, облегчающий настройку поддержки нескольких языков в зависимости от нужд конкретного учреждения.
</li>
<li>
Добавлено дополнение для GOsa² для управления паролями.
</li>
<li>
Улучшена поддержка TLS/SSL во внутренней сети.
</li>
<li>
Настройка Kerberos поддерживает службы NFS и SSH.
</li>
<li>
Доступен инструмент для повторного создания базы данных LDAP.
</li>
<li>
X2Go-сервер устанавливается на все системы с профилем LTSP-сервера.
</li>
</ul>

<h2>Загрузка, шаги установки и руководство</h2>

<p>
Доступны отдельные образы компакт-дисков с сетевым установщиком для 64-битных и
32-битных ПК. 32-битные образы требуются лишь в редких случаях (для ПК старше
12 лет). Образы можно загрузить по следующим
адресам:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Кроме того, доступны расширенные BD-образы (объёмом более
5 ГБ). С их помощью можно настроить всю сеть Debian Edu без подключения
к сети Интернет (образ содержит все окружения рабочего стола и все языки, поддерживаемые
Debian). Эти образы можно загрузить по следующим адресам:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Образы можно проверить, используя подписанные контрольные суммы, размещённые в
каталогах загрузки.
<br />
Как только вы загрузите образ, вы можете проверить, что
</p>

<ul>
<li>
его контрольная сумма совпадает с контрольной суммой из файла, содержащего контрольную сумму; и что
</li>
<li>
файл с контрольной суммой не был подменён в ходе передачи данных.
</li>
</ul>

<p>
Дополнительную информацию о том, как это сделать, можно найти в
<a href="https://www.debian.org/CD/verify">руководстве по проверке</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> полностью построен на основе Debian 10 <q>Buster</q>; поэтому
исходный код для всех пакетов доступен из архива Debian.
</p>

<p>
Обратите внимание на
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">страницу статуса Debian Edu Buster</a>.
Там можно найти актуальную информацию о Debian Edu 10 <q>Buster</q>, включающую инструкции
по использованию <code>rsync</code> для загрузки ISO-образов.
</p>

<p>
При обновлении с Debian Edu 9 <q>Stretch</q> сначала ознакомьтесь с соответствующей
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">главой руководства по Debian Edu</a>.
</p>

<p>
Информацию об установке ищите в соответствующей
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">главе руководства по Debian Edu</a>.
</p>

<p>
После установки вам следует сделать эти
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">первые шаги</a>.
</p>

<p>
За последней версией руководства по Debian Edu <q>Buster</q> обращайтесь к
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">вики Debian Edu</a>.
Руководство было полностью переведено на немецкий, французский, итальянский, датский, нидерландский,
норвежский букмол и японский языки. Частичные переводы имеются для испанского
и упрощённого китайского языков.
Доступен обзор <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
наиболее свежих переводов руководства</a>.
</p>

<p>
Дополнительную информацию о Debian 10 <q>Buster</q> можно найти в информации о
выпуске и руководстве по установке; см. <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a> либо отправив письмо по адресу
&lt;press@debian.org&gt;.</p>
