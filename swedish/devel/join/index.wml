#use wml::debian::template title="Hur du kan gå med i Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="21849af3149d448e7ee39af170c93bee402c4d3a"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Vi söker alltid efter nya bidragslämnare och frivilliga. Vi uppmuntrar starkt att <a href="$(HOME)/intro/diversity">alla deltar</a>. Krav: intresse i fri mjukvara och lite ledig tid.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Läs</a></li>
<li><a href="#contributing">Bidra</a></li>
<li><a href="#joining">Gå med</a></li>
</ul>


<h2><a id="reading">Läs</a></h2>

<p>
Om du inte redan har gjort det, bör du läsa webbsidorna som länkas på <a
href="$(HOME)">Debians startsida</a>. Detta kommer att hjälpa dig att få
bättre förståelse för vilka vi är och vad vi försöker åstadkomma. Som en
potentiell bidragslämnare till Debian, vänligen ägna särskild uppmärksamhet
åt dessa två sidor:
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Debian riktlinjer för fri mjukvara</a></li>
  <li><a href="$(HOME)/social_contract">Debians sociala kontrakt</a></li>
</ul>

<p>
En stor del av vår kommunikation sker på Debians <a
href="$(HOME)/MailingLists/">sändlistor</a>. Om
du vill få en känsla av hur Debianprojektet fungerar på insidan bör då
åtminstone prenumerera på <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
och <a href="https://lists.debian.org/debian-news/">debian-news</a>. Båda
dessa listor har låg volym och dokumenterar vad som är på gång inom gemenskapen.
<a href="https://www.debian.org/News/weekly/">Debians Projektnyheter</a>, som
även publiceras på debian-news-listan summerar nya diskussioner från
Debian-relaterade sändlistor och bloggar, och tillhandahåller länkar till dessa.
</p>

<p>
Som en blivande utvecklare bör du även prenumerera på <a
href="https://lists.debian.org/debian-mentors/">debian-mentors</a>. Här
kan du ställa frågor om paketering och infrastrukturprojekt så väl som andra
utvecklarrelaterade problem. Vänligen notera att denna sändlista är menad för
nya bidragslämnare, inte nya användare. Andra intressanta listor är <a
href="https://lists.debian.org/debian-devel/">debian-devel</a>
(tekniska utvecklingsämnen), <a
href="https://lists.debian.org/debian-project/">debian-project</a>
(diskussioner om icke-tekniska frågor i projektet), <a
href="https://lists.debian.org/debian-release/">debian-release</a>
(koordinering av Debianutgåvor), <a
href="https://lists.debian.org/debian-qa/">debian-qa</a> (kvalitetssäkring).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Sändlisteprenumerationer</a></button></p>

<p>
<strong>Tips:</strong> Om du vill minska antalet e-post du får, speciellt på
listor med hög trafik, erbjuder vi e-postsammandrag som en
sammanfattning av e-post istället för individuella meddelanden. Du kan även
besöka <a href="https://lists.debian.org/">sändlistearkiven</a> för att läsa
meddelandena från våra listor i en webbläsare.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Du behöver inte vara en officiell Debianutvecklare (DD - Debian Develeper) för att bidra. Istället kan en befintlig DD fungera som <a href="newmaint#Sponsor">sponsor</a> och hjälpa till att integrera ditt arbete i projektet.</p>
</aside>

<h2><a id="contributing">Bidra</a></h2>

<p>
Är du intresserad av att underhålla paket? I sådana fall - ta en titt på
vår sida <a href="$(DEVEL)/wnpp/">Hjälp sökes-sida</a> (Eng. Work-needing and
Prospecive Packages). Här kan du hitta paket som behöver en (ny) ansvarig. Att
ta över ett övergivet paket är ett bra sätt att komma igång som paketansvarig.
Det inte bara hjälper vår distribution, men ger dig även möjligheten att lära
dig från den föregående paketansvarige.
</p>

<p>
Här listas några andra idéer hur du kan bidra till Debian:
</p>

<ul>
  <li>Hjälp oss att skriva vår <a href="$(HOME)/doc/">dokumentation</a>.</li>
  <li>Hjälp oss att underhålla <a href="$(HOME)/devel/website/">Debianwebbplatsen</a>, producera innehåll, redigera eller översätt existerande text. </li>
  <li>Gå med i vår <a href="$(HOME)/international/">översättningsgrupp</a>.</li>
  <li>Förbättra Debian och gå med <a href="https://qa.debian.org/">Debians kvalitetssäkringsgrupp (QA)</a>.</li>
</ul>

<p>
Självklart finns det mängder med andra saker du kan göra och vi söker
alltid efter folk för att erbjuda juridiskt stöd eller gå med vår <a
href="https://wiki.debian.org/Teams/Publicity">Debian publicitetsgrupp<a/>.
Att gå med i en Debiangrupp är ett fantastiskt sätt att få erfarenhet, innan
man påbörjar <a href="newmaint">Nymedlemsprocessen</a>. Det är också en bra
startpunkt om du söker efter en paketsponsor. Så hitta en grupp och sätt igång!
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Lista på Debiangrupper</a></button></p>


<h2><a id="joining">Gå med</a></h2>

<p>
Så du har bidragit till Debianprojektet under en viss tid och vill nu
gå med i Debian i en mer officiell roll? Det finns två olika alternativ:
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Rollen som Debian Maintainer (DM) introducerades 2007. Fram till dess var den enda officiella rollen Debianutvecklare (DD, Debian Developer). Det finns för närvarande två oberoende processer för att ansöka om dessa roller.</p>
</aside>

<ul>
  <li><strong>Debian Maintainer (DM):</strong> Detta är det första steget
  - som DM kan du ladda upp din egna paket till Debianarkivet (med några
  restriktioner). Till skillnad från sponsrade paketansvariga, kan Debian
  Maintainers underhålla paket utan en sponsor. <br>Mer information på:
  <a href="https://wiki.debian.org/DebianMaintainer">wikisidan för Debian Maintainers</a>
  </li>
  <li><strong>Debianutvecklare (DD - Debian Developer):</strong> Detta är den
  traditionella fullständiga medlemskapsrollen i Debian. En DD kan delta i
  Debianval. Debianutvecklare med uppladdningsrättigheter kan ladda upp alla
  paket till Debianarkivet. Innan du blir en Debianutvecklare med
  uppladdningsrättigheter bör du ha underhållit paket i åtminstone sex månader
  (exempelvis laddat upp paket som DM, jobbat i en grupp eller underhållt
  paket som laddas upp av sponsorer). Icke-uppladdande DDs har samma
  paketeringsrättighter som Debian Maintainers. Innan du ansöker om en roll som
  icke-uppladdande DD bör du ha en synlig och betydande meritlista rörande
  arbete inom projektet. <br>Mer information: <a 
  href="newmaint">Nymedlemshörnan</a></li>
</ul>

<p>
Oberoende av rollen du väljer att ansöka som, bör du vara bekant med
Debians rutiner. Det är därför som vi starkt rekommenderar att läsa
<a href="$(DOC)/debian-policy/">Debianpolicyn</a> så väl som <a
href="$(DOC)/developers-reference/">Utvecklarreferensen</a>.
</p>
