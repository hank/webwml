#use wml::debian::translation-check translation="546016c2142ac66b3cadc5d6f9f7358a846ad458" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Pavel Toporkov a découvert une vulnérabilité dans Neutron, le service de
réseau virtuel d'OpenStack, qui permettait une reconfiguration de dnsmasq
au moyen de paramètres dhcp_extra_opts contrefaits.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 2:13.0.7+git.2021.09.27.bace3d1890-0+deb10u1. Cette mise à jour
corrige aussi le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-20267">\
CVE-2021-20267</a>.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2:17.2.1-0+deb11u1. Cette mise à jour corrige aussi le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-38598">\
CVE-2021-38598</a>.</p>

<p>Nous vous recommandons de mettre à jour vos paquets neutron.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de neutron, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/neutron">\
https://security-tracker.debian.org/tracker/neutron</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4983.data"
# $Id: $
