#use wml::debian::translation-check translation="3b289c6b6a54e1aeaa5d3778deb6c70daabc6075" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’exécution de code
arbitraire dans <a href="https://gruntjs.com/">Grunt</a>, un exécuteur de tâche
Javascript. Cela était possible à cause d’un chargement non sûr de documents
YAML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7729">CVE-2020-7729</a>

<p>Le paquet grunt avant la version 1.3.0 était vulnérable à une exécution de
code arbitraire à cause de l’utilisation par défaut de la fonction load() au
lieu de sa remplaçante sécurisée safeLoad() du paquet js-yaml dans
grunt.file.readYAML.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.0.1-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets grunt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2368.data"
# $Id: $
