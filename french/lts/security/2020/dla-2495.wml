#use wml::debian::translation-check translation="debc3c1df379154dec13dc7ecb5018ebc1082e19" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’Apache Tomcat dans ses versions 8.5.0 à 8.5.59 pourrait
réutiliser la valeur d’en-tête de requête HTTP du flux précédemment reçu sur une
HTTP/2 pour la requête associée au flux suivant. Quoique que cela devrait
vraisemblablement conduire à une erreur et à la fermeture de la connexion, il
est possible que cette information puisse fuiter entre les requêtes.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 8.5.54-0+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2495.data"
# $Id: $
