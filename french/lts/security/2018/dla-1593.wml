#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Simon Scannell et Robin Peraglie de RIPS Technologies ont découvert que le
passage d’un chemin absolu à une vérification file_exists dans phpBB, un forum
web complet, permettait une exécution de code à distance à l’aide d’une
injection d’objet en employant une désérialisation Phar lorsqu’un attaquant a
accès au panneau de contrôle administrateur avec les permissions du créateur.</p>

<p>La correction pour ce problème consiste dans la suppression du réglage de
chemin d’ImageMagick. La bibliothèque d’images GD peut être utilisée comme
remplacement et un nouvel évènement pour créer des vignettes a été ajouté, de
façon qu’il soit possible d’utiliser une bibliothèque d’images pour créer des
vignettes.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 3.0.12-5+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpbb3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1593.data"
# $Id: $
