#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’interpréteur pour le
langage Ruby.

Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17405">CVE-2017-17405</a>

<p>Une vulnérabilité d’injection de commande dans Net::FTP peut permettre à un
serveur FTP malveillant l’exécution de commandes arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17790">CVE-2017-17790</a>

<p>Une vulnérabilité d’injection de commande dans lazy_initialze
de lib/resolv.rb peut permettre une attaque par injection de commande. Cependant
l’entrée de données non sûres est improbable.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.9.3.194-8.1+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby1.9.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1221.data"
# $Id: $
