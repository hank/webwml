#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été détectées dans graphicsmagick,
un ensemble d’utilitaires de traitement et de bibliothèques d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13134">CVE-2017-13134</a>

<p>Graphicsmagick était vulnérable à une lecture hors limites de tampon basé sur
le tas et à un déni de service à l'aide d'un fichier SFW contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16547">CVE-2017-16547</a>

<p>Graphicsmagick était vulnérable à un déni de service distant (plantage
d'application) ou à un autre impact non précisé à l'aide d'un fichier contrefait
résultant d’une allocation de mémoire déficiente.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.3.16-1.1+deb7u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1170.data"
# $Id: $
