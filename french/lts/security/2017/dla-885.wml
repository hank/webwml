#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans python-django, un cadriciel de
développement web de haut niveau en Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7233">CVE-2017-7233</a> (#859515)

<p>Attaque de redirection ouverte et attaque XSS possible à l’aide de
redirections d’URL numérique fournies par l’utilisateur. Django s’appuie sur
l’entrée d’utilisateur dans quelques cas (par exemple,
django.contrib.auth.views.login() et i18n) pour rediriger l’utilisateur vers une
URL <q>on success</q>. La vérification de sécurité pour ces redirections
(à savoir is_safe_url()) considérait une URL numérique (par exemple,
http:999999999) <q>safe</q> alors que cela ne devait pas être le cas. Aussi, si
un développeur comptait sur is_safe_url() pour fournir des cibles de redirection
sûres et mettait de telles URL dans des liens, il pourrait subir une
attaque XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7234">CVE-2017-7234</a> (#895516)

<p>Vulnérabilité de redirection ouverte dans django.views.static.serve. Une URL
contrefaite et malveillante vers un site Django utilisant le visualisateur
serve() pourrait rediriger vers n’importe quel autre domaine. Le visualisateur
ne redirige plus car il ne fournit aucune fonction connue utile.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 1.4.22-1+deb7u3 de python-django.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-885.data"
# $Id: $
