<define-tag pagetitle>Ampere donates Arm64 server hardware to Debian to fortify the Arm ecosystem</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
<a href="https://amperecomputing.com/">Ampere®</a> has partnered with Debian
to support our hardware infrastructure through the donation
of three of Ampere's high-performance Arm64 servers. 
These Lenovo ThinkSystem HR330A servers contain Ampere's eMAG CPU with an
Arm®v8 64-bit processor specifically designed for cloud servers, 
equipped with 256GB RAM, dual 960GB SSDs, and a 25GbE dual port NIC. 
</p>

<p>
The donated servers have been deployed at the University of British Columbia,
our hosting partner in Vancouver, Canada.
The Debian System Administrators (DSA) have configured them to run
arm64/armhf/armel build daemons, replacing the build daemons running on less
powerful development-grade boards. On virtual machines with half as many
allocated vCPUs, the result has been that the time to build Arm* packages has
been halved with Ampere's eMAG system. Another benefit from this generous gift
is that it will allow DSA to migrate some general Debian services currently operating in
our present infrastructure, and will provision virtual machines for other
Debian teams (e.g.: Continuous Integration, Quality Assurance, etc.)
who require access to Arm64 architecture. 
</p>

<p>
<q>Our partnership with Debian supports our developer strategy to expand
the open source communities which run on Ampere servers to further build out
the Arm64 ecosystem and enable the creation of new applications,</q>
said Mauri Whalen, vice president of software engineering at Ampere.
<q>Debian is a well-run and respected community, and we are proud to work with them.</q>
</p>

<p>
<q>The Debian System Administrators are grateful to Ampere for the donation of
carrier-grade Arm64 servers. Having servers with integrated standard management
interfaces such as Intelligent Platform Management Interface (IPMI),
and with Lenovo's hardware warranties and support organization behind them,
is precisely what DSA has been wanting for the Arm64 architecture.
These servers are very powerful and very well equipped: we anticipate using them
for general services in addition to Arm64 build daemons. I think they'll prove
to be very appealing to cloud operators and I'm thrilled that Ampere Computing
has partnered with Debian.</q> - Luca Filipozzi, Debian System Administrator.
</p>

<p>
It is only through the donation of volunteer effort, in-kind equipment and
services, and funding that Debian is able to deliver on our commitment of a
free operating system. We are very appreciative of Ampere's generosity.
</p>

<h2>About Ampere Computing</h2>
<p>
Ampere is designing the future of hyperscale cloud and edge computing with the
world's first cloud native processor. Built for the cloud with a modern 64-bit
Arm server-based architecture, Ampere gives customers the freedom to accelerate
the delivery of all cloud computing applications. With industry-leading cloud
performance, power efficiency and scalability, Ampere processors are tailored
for the continued growth of cloud and edge computing.
</p>

<h2>About Debian</h2>
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>


<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>
