<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>jQuery mishandles <code>jQuery.extend(true, {}, ...)</code> because of 
<code>Object.prototype</code>
pollution.  If an unsanitized source object contained an enumerable <code>__proto__</code>
property, it could extend the native <code>Object.prototype</code>. For additional
information, please refer to the upstream advisory at
<a href="https://www.drupal.org/sa-core-2019-006">https://www.drupal.org/sa-core-2019-006</a> .</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.7.2+dfsg-3.2+deb8u6.</p>

<p>We recommend that you upgrade your jquery packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1777.data"
# $Id: $
