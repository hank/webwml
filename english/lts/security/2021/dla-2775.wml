<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in plib.</p>

<p>Integer overflow vulnerability that could result in arbitrary code execution.
The vulnerability is found in ssgLoadTGA() function in src/ssg/ssgLoadTGA.cxx file.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.8.5-7+deb9u1.</p>

<p>We recommend that you upgrade your plib packages.</p>

<p>For the detailed security status of plib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/plib">https://security-tracker.debian.org/tracker/plib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2775.data"
# $Id: $
