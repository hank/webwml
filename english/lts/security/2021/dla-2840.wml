<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that roundcube, a skinnable AJAX based webmail solution
for IMAP servers, did not properly sanitize requests and mail messages.
This would allow an attacker to perform Cross-Side Scripting (XSS) or SQL
injection attacks.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.2.3+dfsg.1-4+deb9u9.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>For the detailed security status of roundcube please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/roundcube">https://security-tracker.debian.org/tracker/roundcube</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2840.data"
# $Id: $
