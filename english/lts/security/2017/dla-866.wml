<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libxslt is vulnerable to an integer overflow in the xsltAddTextString
function that can be exploited to trigger an out of bounds write on 64-bit
systems.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
1.1.26-14.1+deb7u3.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-866.data"
# $Id: $
