#use wml::debian::template title="People: Who we are and what we do" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">How it all started<a>
    <li><a href="#devcont">Developers and Contributors</a>
    <li><a href="#supporters">Individuals and Organizations supporting Debian</a>
    <li><a href="#users">Debian Users</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Since many people have asked: Debian is pronounced <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> It's named after the creator of Debian, Ian Murdock, and his wife, Debra.</p>
</aside>

<h2><a id="history">How it all started</a></h2>

<p>It was in August 1993, when Ian Murdock started working on a new
operating system which would be made openly, in the spirit of Linux and
GNU. He sent out an open invitation to other software developers, asking
them to contribute to a software distribution based on the Linux kernel,
which was relatively new back then. Debian was meant to be carefully
and conscientiously put together, and to be maintained and supported
with similar care, embracing an open design, contributions, and support
from the Free Software community.</p>

<p>It started as a small, tightly-knit group of Free Software hackers and
gradually grew to become a large, well-organized community of developers,
contributors, and users.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Read the complete History</a></button></p>

<h2><a id="devcont">Developers and Contributors</a></h2>

<p>
Debian is an all-volunteer organization. More than thousand active
developers spread <a href="$(DEVEL)/developers.loc">around the world</a>
work on Debian in their spare time. Few of us have actually met in
person. Instead, we communicate primarily through email (mailing lists
at <a href="https://lists.debian.org/">lists.debian.org</a>) and IRC
(channel #debian at irc.debian.org).
</p>

<p>
The complete list of official Debian members can be found on
<a href="https://nm.debian.org/members">nm.debian.org</a>, and
<a href="https://contributors.debian.org">contributors.debian.org</a>
shows a list of all contributors and teams who work on the
Debian distribution.</p>

<p>The Debian Project has a carefully <a href="organization">organized
structure</a>. For more information on how the Debian project looks from the inside,
please visit the <a href="$(DEVEL)/">Developers' Corner</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Read about our Philosophy</a></button></p>

<h2><a id="supporters">Individuals and Organizations supporting Debian</a></h2>

<p>Apart from developers and contributors, many other individuals and
organizations are part of the Debian community:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting and hardware sponsors</a></li>
  <li><a href="../mirror/sponsors">Mirror sponsors</a></li>
  <li><a href="../partners/">Development and service partners</a></li>
  <li><a href="../consultants">Consultants</a></li>
  <li><a href="../CD/vendors">Vendors of Debian installation media</a></li>
  <li><a href="../distrib/pre-installed">Computer vendors offering pre-installed Debian machines</a></li>
  <li><a href="../events/merchandise">Merchandise vendors</a></li>
</ul>

<h2><a id="users">Debian Users</a></h2>

<p>
Debian is used by a wide range of organizations, large and small, as well
as many thousands of individuals. See our <a href="../users/">Who's using
Debian?</a> page for a list of educational, commercial, and non-profit
organizations as well as government agencies which have submitted short
descriptions of how and why they use Debian.
</p>
