<define-tag pagetitle>Debian 11 aktualisiert: 11.1 veröffentlicht</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Erik Pfannenstein"

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die erste Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung bringt hauptsächlich Korrekturen für
Sicherheitsprobleme und Anpassungen für einige ernste Probleme. Für sie sind
bereits separate Sicherheitsankündigungen veröffentlicht worden; auf diese
wird, wo möglich, verwiesen.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version
von Debian <release> darstellt, sondern nur einige der enthaltenen Pakete
auffrischt. Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da
deren Pakete nach der Installation mit Hilfe eines aktuellen
Debian-Spiegelservers auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apr "Array-Dereferenzierung außerhalb der Grenzen verhindern">
<correction atftp "Pufferüberlauf behoben [CVE-2021-41054]">
<correction automysqlbackup "Absturz bei Verwendung von <q>LATEST=yes</q> beseitigt">
<correction base-files "Aktualisierung auf die ZWsichenveröffentlichung 11.1">
<correction clamav "Neue Veröffentlichung der Originalautoren; Speicherzugriffsfehler behoben, der in clamdscan auftritt, wenn --fdpass und --multipass zusammen mit ExcludePath verwendet werden">
<correction cloud-init "Doppeltes »includedir« in /etc/sudoers vermeiden">
<correction cyrus-imapd "Dienstblockade beseitigt [CVE-2021-33582]">
<correction dazzdb "Use-after-free in DBstats behoben">
<correction debian-edu-config "debian-edu-ltsp-install: Hauptserver-bezogene Ausschlussliste erweitern; slapd und xrdp-sesman zur Liste der maskierten Dienste hinzugefügt">
<correction debian-installer "Neukompilierung gegen proposed-updates; Linux-ABI auf 5.10.0-9 aktualisiert; udebs aus proposed-updates verwenden">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates; udebs aus proposed-updates und Stable verwenden; xz-komprimierte Paketdateien verwenden">
<correction detox "Umgang mit großen Dateien verbessert">
<correction devscripts "Dafür gesorgt, dass die --bpo-Option bullseye-backports ansteuert">
<correction dlt-viewer "Fehlende Headerdateien qdlt/qdlt*.h ins Dev-Paket aufgenommen">
<correction dpdk "Neue Veröffentlichung der Originalautoren">
<correction fetchmail "Speicherzugriffsfehler und Sicherheitsregression behoben">
<correction flatpak "Neue Veröffentlichung der Originalautoren; keine unüblichen $XDG_RUNTIME_DIR-Einstellungen in die Sandbox übernehmen">
<correction freeradius "Thread-Crash behoben und Beispielskonfiguration überarbeitet">
<correction galera-3 "Neue Veröffentlichung der Originalautoren">
<correction galera-4 "Neue Veröffentlichung der Originalautoren; kreislaufende Konflikte mit galera-3 durch Abschaffung des virtuellen <q>galera</q>-Paketes behoben">
<correction glewlwyd "Möglichen Pufferüberlauf während der FIDO2-Signaturverifizierung in der webauthn-Registrierung verhindert [CVE-2021-40818]">
<correction glibc "openssh-server auch dann neu starten, wenn er während des Upgrades dekonfiguriert worden ist; Text-Fallback bei Nichtverfügbarkeit von debconf überarbeitet">
<correction gnome-maps "Neue Veröffentlichung der Originalautoren; Absturz beim Starten behoben, wenn die zuletzt verwendete Karte eine Luftkarte ist, aber keine Luftkarten-Kacheldefinition gefunden wurde; aufhören, beim Verlassen gelegentlich unbrauchbare Zuletzt-Betrachtet-Positionen zu speichern; Hänger beim Verschieben vom Routenmarkierungen behoben">
<correction gnome-shell "Neue Veröffentlichung der Originalautoren; Einfrieren nach dem Abbrechen (mancher) System-Modal-Dialoge behoben; Wortvorschläge der Bildschirmtastatur überarbeitet; Abstürze behoben">
<correction hdf5 "Paketabhängigkeiten nachjustiert, um Upgrade-Pfade von älteren Veröffentlichungen zu ebnen">
<correction iotop-c "Richtig mit UTF-8-Prozessnamen umgehen">
<correction jailkit "Erzeugungsroutine von Jails, die /dev verwenden müssen, überarbeitet; Prüfung auf Vorhandensein von Bibilotheken korrigiert">
<correction java-atk-wrapper "Auch den dbus verwenden, um festzustellen, ob Barrierefreieheit verwendet wird">
<correction krb5 "KDC-Absturz wegen Nullzeiger-Dereferenzierung bei FAST-Anfragen ohne Server-Feld behoben [CVE-2021-37750]; Speicherleck in krb5_gss_inquire_cred gestopft">
<correction libavif "Richtige libdir in der libavif.pc pkgconfig-Datei benutzen">
<correction libbluray "Wechsel auf eingebettete libasm; die Version aus libasm-java ist zu neu">
<correction libdatetime-timezone-perl "Neue Veröffentlichung der Originalautoren; Sommerzeitregeln für Samoa and Jordanien aktualisiert; Bestätigung der nicht stattfindenden Schaltsekunde am 31.12.2021">
<correction libslirp "Mehrere Probleme mit Pufferüberläufen behoben [CVE-2021-3592 CVE-2021-3593 CVE-2021-3594 CVE-2021-3595]">
<correction linux "Neue Veröffentlichung der Originalautoren; ABI auf 9 angehoben; [rt] Aktualisierung auf 5.10.65-rt53; [mipsel] bpf, mips: Abweichungen der Bedingungszweige validieren [CVE-2021-38300]">
<correction linux-signed-amd64 "Neue Veröffentlichung der Originalautoren; ABI auf 9 angehoben; [rt] Aktualisierung auf 5.10.65-rt53; [mipsel] bpf, mips: Abweichungen der Bedingungszweige validieren [CVE-2021-38300]">
<correction linux-signed-arm64 "Neue Veröffentlichung der Originalautoren; ABI auf 9 angehoben; [rt] Aktualisierung auf 5.10.65-rt53; [mipsel] bpf, mips: Abweichungen der Bedingungszweige validieren [CVE-2021-38300]">
<correction linux-signed-i386 "Neue Veröffentlichung der Originalautoren; ABI auf 9 angehoben; [rt] Aktualisierung auf 5.10.65-rt53; [mipsel] bpf, mips: Abweichungen der Bedingungszweige validieren [CVE-2021-38300]">
<correction mariadb-10.5 "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2021-2372 CVE-2021-2389]">
<correction mbrola "Dateiende-Erkennung überarbeitet">
<correction modsecurity-crs "Probleme mit Einschleusung via Anfragekörper behoben [CVE-2021-35368]">
<correction mtr "Regression in der JSON-Ausgabe behoben">
<correction mutter "Neue Veröffentlichung der Originalautoren; kms: Umgang mit gängigen Videomodi, welche die mögliche Bandbreite überschreiten können, verbessert; gültige Fenstertexturgröße nach Viewport-Änderungen sicherstellen">
<correction nautilus "Aufhören, mehrere gleichzeitig ausgewählte Dateien in mehreren Anwendungsinstanzen zu öffnen; Fenstergröße und -position beim Tiling nicht speichern; diverse Speicherlecks gestopft; Übersetzungen aktualisiert">
<correction node-ansi-regex "Auf reguläre Ausdrücke basierende Dienstblockade behoben [CVE-2021-3807]">
<correction node-axios "Auf reguläre Ausdrücke basierende Dienstblockade behoben [CVE-2021-3749]">
<correction node-object-path "Probleme mit Prototyp-Pollution behoben [CVE-2021-23434 CVE-2021-3805]">
<correction node-prismjs "Auf reguläre Ausdrücke basierende Dienstblockade behoben [CVE-2021-3801]">
<correction node-set-value "Prototyp-Pollution behoben [CVE-2021-23440]">
<correction node-tar "Verzeichnis-Cache von Nicht-Verzeichnissen säubern [CVE-2021-32803]; absolute Pfade gründlicher stutzen [CVE-2021-32804]">
<correction osmcoastline "Nicht-WGS84-Projektionen verbessert">
<correction osmpbf "Neukompilierung gegen protobuf 3.12.4">
<correction pam "Syntaxfehler in libpam0g.postinst, der auftritt, wenn eine systemd-Unit fehlschlägt, behoben">
<correction perl "Sicherheitsaktualisierung; Speicherleck in Zusammenhang mit regulären Ausdrücken behoben">
<correction pglogical "Übernahme der Korrekturen für die Snapshot-Handhabung von PostgreSQL 13.4">
<correction pmdk "Fehlende Barrieren nach non-temporal memcpy wieder hinzugefügt">
<correction postgresql-13 "Neue Veröffentlichung der Originalautoren; Fehlplanung der wiederholten Anwendung eines Projektionsschritts behoben [CVE-2021-3677]; SSL-Wiederverhandlung vollständiger verbieten">
<correction proftpd-dfsg "<q>mod_radius leaks memory contents to radius server</q> und <q>sftp connection aborts with </q>Corrupted MAC on input"" behoben; Escaping von bereits maskiertem SQL-Text überspringen">
<correction pyx3 "Probleme mit horizontaler Schriftausrichtung in texlive 2020 behoben">
<correction reportbug "Suite-Namen der Bullseye-Veröffentlichung entsprechend aktualisiert">
<correction request-tracker4 "Timing-Seitenkanal im Anmeldesystem geschlossen [CVE-2021-38562]">
<correction rhonabwy "JWE-CBC-Tag-Berechnung und JWS-alg:none-Signaturverifizierung überarbeitet">
<correction rpki-trust-anchors "HTTPS-URL zum LACNIC TAL hinzugefügt">
<correction rsync "--copy-devices wieder eingeführt; Regression in --delay-updates behoben; Anpassungen für Grenzfall in --mkpath vorgenommen; rsync-ssl überarbeitet; --sparce und --inplace überarbeitet; für rrsync verfügbare Optionen aktualisiert; Korrekturen in der Dokumentation">
<correction ruby-rqrcode-rails3 "Korrektur für Kompatibilität zu ruby-rqrcode 1.0">
<correction sabnzbdplus "Verzeichnisausbruch in der Umbenennungs-Funktion verhindert [CVE-2021-29488]">
<correction shellcheck "Darstellung langer Optionen auf Handbuchseite verbessert">
<correction shiro "Probeme mit Authentifizierungs-Umgehung behoben [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; Korrektur für Spring-Framework-Kompatibilität eingespielt; Guice 4 unterstützen">
<correction speech-dispatcher "Festlegung des Stimmen-Namen für das generische Modul überarbeitet">
<correction telegram-desktop "Absturz, wenn auto-delete eingeschaltet ist, abgestellt">
<correction termshark "Themen in Paket aufgenommen">
<correction tmux "Race Condition behoben, die dafür sorgt, dass die Konfiguration nicht geladen wird, wenn mehrere Clients während der Initialisierung mit dem Server interagieren">
<correction txt2man "Regression im Umgang mit Display-Blöcken behoben">
<correction tzdata "Sommerzeitregeln für Samoa und Jordanien aktualisiert; Bestätigung der nicht stattfindenden Schaltsekunde am 31.12.2021">
<correction ublock-origin "Neue Veröffentlichung der Originalautoren; Dienstblockade behoben [CVE-2021-36773]">
<correction ulfius "Vor der Verwendung von Speicher sicherstellen, dass er initialisiert wird [CVE-2021-40540]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4959 thunderbird>
<dsa 2021 4960 haproxy>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4965 libssh>
<dsa 2021 4966 gpac>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4968 haproxy>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4972 ghostscript>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4976 wpewebkit>
<dsa 2021 4977 xen>
<dsa 2021 4978 linux-signed-amd64>
<dsa 2021 4978 linux-signed-arm64>
<dsa 2021 4978 linux-signed-i386>
<dsa 2021 4978 linux>
<dsa 2021 4979 mediawiki>
</table>

<p>
Während der letzten Abschnitte des Bullseye-Freeze sind
über <a href="https://security.debian.org/">das Sicherheits-Archiv</a>
einige Aktualisierungen ausgespielt worden, aber ihnen ist keine DSA zugeordnet.
Hier sind die Details zu diesen Aktualisierungen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apache2 "mod_proxy HTTP2 Anfragezeileninjektion abgestellt [CVE-2021-33193]">
<correction btrbk "Eigenmächtige Codeausführung verhindert [CVE-2021-38173]">
<correction c-ares "Fehlende Eingabe-Verifizierung von Hostnamen, die von den DNS-Servern zurückgeliefert werden, nachgetragen [CVE-2021-3672]">
<correction exiv2 "Überläufe behoben [CVE-2021-29457 CVE-2021-31292]">
<correction firefox-esr "Neue stabile Veröffentlichung der Originalautoren [CVE-2021-29980 CVE-2021-29984 CVE-2021-29985 CVE-2021-29986 CVE-2021-29988 CVE-2021-29989]">
<correction libencode-perl "Encodieren: @INC-Pollution beim Laden von ConfigLocal umgangen [CVE-2021-36770]">
<correction libspf2 "spf_compile.c: Richtige Größe von ds_avail [CVE-2021-20314]; <q>reverse</q>-Makro-Modifizierer überarbeitet">
<correction lynx "Leckage von Zugangsdaten abgestellt, die auftrat, wenn SNI zusammen mit einem URL verwendet wurde, der die Zugangsdaten enthielt [CVE-2021-38165]">
<correction nodejs "Neue Version der Originalautoren; Use-after-free beseitigt [CVE-2021-22930]">
<correction tomcat9 "Möglichkeit zur Umgehung der Authentifizierung [CVE-2021-30640] und des Anfrageschmuggels [CVE-2021-33037] entfernt">
<correction xmlgraphics-commons "Serverseitige Abfragefälschung verhindert [CVE-2020-11988]">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt; oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>
