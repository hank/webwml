#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

#use wml::debian::translation-check translation="fe2d0a6290a91814fb8d336efc57e158b56b319e"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>De Gemeenschap</h1>
      <h2>Debian is een gemeenschap van mensen!</h2>

#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Mensen</a></h2>
        <p>Wie we zijn en wat we doen</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Onze filosofie</a></h2>
        <p>Waarom we het doen, en hoe we het doen</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Meewerken, bijdragen</a></h2>
        <p>Hoe u zich bij ons kunt aansluiten!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
         <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Meer ...</a></h2>
        <p>Extra informatie over de Debian-gemeenschap</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Het besturingssysteem</h1>
      <h2>Debian is een volledig vrij besturingssysteem!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Waarom Debian?</a></h2>
        <p>Wat maakt Debian speciaal?</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Gebruikersondersteuning</a></h2>
        <p>Hulp krijgen en documentatie</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Beveiligingsupdates</a></h2>
        <p>Beveiligingsaanbevelingen van Debian (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Meer ...</a></h2>
        <p>Meer links naar downloads en software</p>
      </div>
    </div>
  </div>
</div>

<hr>

# <!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <div class="row">
#  <div class="column styled-href-blue column-left">
#    <div style="text-align: center">
#      <h2><a href="https://debconf21.debconf.org/">DebConf21</a> is underway!</h2>
#      <p>The Debian Conference is being held online from August 24 to August 28, 2021.</p>
#    </div>
#  </div>
# </div>


<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Nieuws en aankondigingen over Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Alle nieuws</a>  &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Nieuws van Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Nieuws van het Debian-project" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Aankondigingen van Debian beveiligingen (enkel titels)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Aankondigingen van Debian beveiligingen (samenvattingen)" href="security/dsa-long">
:#rss#}

